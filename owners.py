'''
For the description of home owners and business owners, and their
behaviours towards purchasing solar panels and batteries, and 
grid defection.
'''
from __future__ import division
from griddefection import *
import datetime
from importSurvey import buy_logit
import outputs
import random
#from PVAdoptionSim import CurrentScenario, PVAdoptionkW, PVAdoptionNum, BatteryAdoption
import pvbattsys

class HomeOwner:
    
    def __init__(self, ID, surveyVars, PVBattSysObj, Load, socNet, T): 
        
        self.ID = ID
        self.survey_ = surveyVars #A list/object containing important variables from the survey
        self.PVBatt = PVBattSysObj
        self.load = Load
        self.network = socNet
#         if surveyVars[7] ==5 :
#             self.T = random.randint(outputs.Ts[0][0],outputs.Ts[0][1])/100
#         elif surveyVars[7] == 4:
#             self.T = random.randint(outputs.Ts[1][0],outputs.Ts[1][1])/100
#         else:
#             self.T = random.randint(outputs.Ts[2][0],outputs.Ts[2][1])/100
        self.T = T#-(surveyVars[7]-4)/5 * outputs.TScale
        #print self.T
        self.withSolarfrac = 0
        #self.actualLoad = []
    
    def UpdateSocialPV(self, AllHomeOwners):
        
        withPV = 0
        for ho in self.network:
            if AllHomeOwners[ho].PVBatt:
                withPV+=1
                
        self.withSolarfrac = withPV/len(self.network)
        
            
    def buyPV(self, CurrentScenario):
        
        PVCapacity = 0
        Battery = 0
#         for pvsys in self.PVBatt:
#             PVCapacity += pvsys.PV
#             Battery += pvsys.battery
            
        if not self.PVBatt and self.withSolarfrac >= self.T:#PVCapacity < HomePVLimit:
            if self.PVBatt !=0:
                print 'pvsystem:', self.PVBatt
            #First choose suitable PV/Battery Capacity(s) 
            addPV = HomePVLimit - PVCapacity
            if addPV > UnitPV:
                pvOptions = [UnitPV, UnitPV*2, max(HomePVLimit, UnitPV*3)]
                
            if CurrentScenario.name == 'validation':
                battOptions= [0]
            else:
                battOptions = [0, Battery + UnitBattery, Battery + UnitBattery*2]
            
            SystemSpecs = []
            PurchaseVars = []
            
            for battopt in battOptions:
                for pvopt in pvOptions:
                    
                    
                    SystemSpecs.append([pvopt,battopt])
                    #this gives [system price, payback, annualROI]
                    PurchaseVars.append(CurrentScenario.purchaseSavings(pvopt, self.load, battopt) + [pvopt, battopt, 'FiT']) #Payback if FiT is used
                    PurchaseVars.append(CurrentScenario.paybackNM(pvopt, self.load, battopt) + [pvopt, battopt,'NM']) #Payback if net metering is used
            
            #sysno = 0
            
            
            #Yes =0, No = 1
            selSys = []
            
            for pvars in PurchaseVars:
                
                if pvars[4]:
                    battIncluded = 1
                else:
                    battIncluded = 0
                
                #This will change as variables change
                if pvars[1]>0 and pvars[1] <=20: #FiT contracts last 20 years, and PV systems are expected to last for 20 - 30 years
                    prediction = buy_logit.predict(exog=dict(Payback=pvars[1], Battery = battIncluded\
                                                         , Cost = pvars[0]*ConversionRate, MaxPay = self.survey_[2], \
                                                         PVDeflection = self.survey_[0], Env2 = self.survey_[3])) 
                    outputs.SystemPaybacks[outputs.CurrentDate+1] += pvars[1]
                    outputs.PurchaseAtt[outputs.CurrentDate+1] += 1
#                 if pvars[1] < 0:
#                     print pvars[1], prediction
#                 
                    if prediction <= 0.5 and pvars[2] != 0: #0.5 threshold
                        #print prediction
                        selSys.append(pvars[3:6])
                        
                #sysno+=1
            
            #Buy one system
            if selSys:
                finalSys = random.choice(selSys)
                
                
                #if not self.PVBatt:
#                 outputs.PVAdoptionNum[outputs.CurrentDate+1] +=1
                #Create PV-batt class
                newSys = pvbattsys.PVBattSystem(finalSys[0], finalSys[1], outputs.CurrentDate+1, finalSys[2])
                self.PVBatt = newSys
                
                #outputs.PVAdoptionNum[outputs.CurrentDate+1] +=1
#                 outputs.PVAdoptionkW[outputs.CurrentDate+1] += finalSys[0]
#                 outputs.BatteryAdoptionkWh[outputs.CurrentDate+1] += finalSys[1]
#                 outputs.BatteryAdoptionNum[outputs.CurrentDate+1] += 1
                
                
                #Log in purchase to env variables
                
              

#     def networkAdoption(self):
#         withSolar = 0 #friends, neighbours with solar
#         for netid in self.network:
#             if outputs.HomeOwnerList[netid].PVBatt:
#                 withSolar +=1
#         
#         self.withSolarfrac = withSolar/len(self.network)
        

    
    def PVBattLoad(self, CurrentScenario):
    

        startmonth = (outputs.CurrentDate*Epoch + 1)%12
        endmonth = ((outputs.CurrentDate+1)*Epoch+1)%12
        if endmonth ==1:
            endyear = 2016
        else:
            endyear = 2015
        
        CalcStart = dt.datetime(2015,startmonth,1,0)
        x = CalcStart - dt.datetime(2015,1,1,0) 
        y = dt.datetime(endyear,endmonth,1,0) - dt.datetime(2015,1,1,0)
        
        starthr = x.days * 24
        endhr = y.days * 24
        
        #Calculate net load with pv generation and bettery
        if self.PVBatt:
#             for i in range(len(self.PVBatt)):
#                 if i == 0:
#                     generation = self.PVBatt[i].elecGen()
#                 else:
            generation = self.PVBatt.elecGen()
            
            
#             batterySize = 0
#             PVSize = 0
#             for pvb in self.PVBatt:
            batterySize = self.PVBatt.battery
            PVSize = self.PVBatt.PV
              

            
            battUsable = batterySize * (1-DoD)
            actualLoad = []  
            for hr in range(starthr, endhr):
                
                
                if CalcStart.month in Summer:
                    ToU = CurrentScenario.ToU[0] #Summer ToU
                else:
                    ToU = CurrentScenario.ToU[1] #Winter ToU
                
                '''The next code block has been modified such that there's no ToU pricing involved'''
                if ToU[CalcStart.hour] == max(ToU) and CalcStart.weekday() not in Weekend: 
                #Use battery
                    availableEnergy = battUsable * BatteryDischargeEff * BatteryDischargeRate 
                    if availableEnergy >= self.load[hr]:
                    
                        battUsable -= self.load[hr] *BatteryDischargeEff
                        
                        actualLoad.append(0)
                    else: #Change has to be made if BatteryDischargeRate <1
                        actualLoad.append(self.load[hr] - battUsable)
                        battUsable = 0
                else:
                    #Charge battery
                    hourCharge = BatteryChargeRate * batterySize
                    oldbattUsable = battUsable
                    battUsable = min(batterySize * (1-DoD), battUsable + hourCharge)
                    actualLoad.append(self.load[hr] + (battUsable - oldbattUsable))
    
        
               
                #outputs.ActualLoads[self.ID] += actualLoad
                CalcStart += dt.timedelta(minutes = 60)
                
            #netLoadOnGrid = np.array(actualLoad) - generation[starthr:endhr]
#            outputs.NetLoadOnGrid[starthr:endhr] += netLoadOnGrid
            return np.array(actualLoad), generation[starthr:endhr]
        else:
            #outputs.NetLoadOnGrid[starthr:endhr] += np.array(self.load[starthr:endhr])
            return np.array(self.load[starthr:endhr]), 0
    
    def PVBattLoad_(self, CurrentScenario):


        startmonth = (outputs.CurrentDate * Epoch + 1) % 12
        endmonth = ((outputs.CurrentDate + 1) * Epoch + 1) % 12
        if endmonth == 1:
            endyear = 2016
        else:
            endyear = 2015

        CalcStart = dt.datetime(2015, startmonth, 1, 0)
        x = CalcStart - dt.datetime(2015, 1, 1, 0)
        y = dt.datetime(endyear, endmonth, 1, 0) - dt.datetime(2015, 1, 1, 0)

        starthr = x.days * 24
        endhr = y.days * 24

        #Calculate net load with pv generation and bettery
        if self.PVBatt:
            #             for i in range(len(self.PVBatt)):
            #                 if i == 0:
            #                     generation = self.PVBatt[i].elecGen()
            #                 else:
            generation = self.PVBatt.elecGen()


            #             batterySize = 0
            #             PVSize = 0
            #             for pvb in self.PVBatt:
            batterySize = self.PVBatt.battery
            PVSize = self.PVBatt.PV

            battUsable = batterySize * (1 - DoD)
            actualLoad = []
            netLoadOnGrid = []
            for hr in range(starthr, endhr):

                RenEnergy = generation[hr]
                if RenEnergy <= self.load[hr]:
                    RestLoad = self.load[hr] - RenEnergy
                    #actualLoad.append(RestLoad)
                    availableEnergy = battUsable * BatteryDischargeEff * BatteryDischargeRate
                    if availableEnergy >= RestLoad:
                        battUsable -= RestLoad * BatteryDischargeEff
                        netLoadOnGrid.append(0)
                    else:
                        #actualLoad.append(RestLoad-battUsable)
                        netLoadOnGrid.append(RestLoad - battUsable)
                        battUsable = 0

                else:
                    RestRen = RenEnergy - self.load[hr]
                    #actualLoad.append(-RestLoad)
                    oldbattUsable = battUsable
                    hourCharge = BatteryChargeRate * batterySize
                    battUsable = min(batterySize * (1 - DoD), battUsable + hourCharge, battUsable + RestRen)
                    Differ = battUsable - oldbattUsable
                    #netLoadOnGrid.append(Differ-RestRen)
                    netLoadOnGrid.append(-RestRen)
                #outputs.ActualLoads[self.ID] += actualLoad
                CalcStart += dt.timedelta(minutes=60)
                #netLoadOnGrid = np.array(actualLoad)
            #            outputs.NetLoadOnGrid[starthr:endhr] += netLoadOnGrid
            #             plt.plot(tempgen)
            #             plt.show()
            #print starthr, endhr
            return np.array(netLoadOnGrid), generation[starthr:endhr]
        else:
            #outputs.NetLoadOnGrid[starthr:endhr] += np.array(self.load[starthr:endhr])
            return np.array(self.load[starthr:endhr]), 0    
      
class BusinessOwner:
    
    def __init__(self, ID, revenue, environmentalConcern, T_solar, T_disconnect, prevSolar,
                 load, sentiments, expert): 
        
        self.ID = ID
        self.revenue = revenue #This is the income category the home owner falls into
        self.envCon = environmentalConcern
        self.T_solar = T_solar #social threshold for purchasing solar
        self.T_disconnect = T_disconnect #social threshold for disconnecting from grid
        self.prevSolar = prevSolar #0 or 1 based on previously owning solar panels 
        self.load = load #Electrical load series, over 8760 hours
        self.solarExpert = expert #Level of knowledge of solar panels
        self.sentiments = sentiments #Array of ACT impressions towards solar panels etc.
        
        

