from __future__ import division
import surveyScenarios
from griddefection import *#FiTlowPV, FiTmidPV, SummerToU, WinterToU
import pickle
import numpy as np
import csv
import pylab as pl


#Collect load data from file
# with open('AllLoads', 'rb') as f:
#     AllLoads = pickle.load(f)
#print len(AllLoads)
# with open('feasibleConfigs', 'rb') as f:
#     feasibleConfigs = pickle.load(f)

#db = mdb.connect(host="typhoon.cs.uwaterloo.ca", user="a2adepet", passwd="Adeda12!", db="essex_annotated")
 
#cur = db.cursor()

#Collect 10 sample business MeterIDs
# cur.execute("select MeterID from meter_business_type")
# BusinessMeters = []
# for row in cur.fetchall():
#     BusinessMeters.append(row[0])
# BusMeterID= np.array(BusinessMeters)

#Collect 10 sample residential MeterIDs
cur.execute("select MeterID from meter_res_structure_type limit 150")
ResidentialMeters = []
for row in cur.fetchall():
    ResidentialMeters.append(row[0])
ResMeterID= np.array(ResidentialMeters)
                     
  
ToUTimes= np.linspace(1,4,7)
FiTTimes = np.linspace(1,10,10)
PVPriceTimes = np.linspace(1/2,1,6)
BattPriceTimes = np.linspace(1/5,1,10)

fit=1
pvp=1
bpt =1
tou =1

Paybacks= []
DSaved = []
Base = []
#for fit in FiTTimes:
#for bpt in BattPriceTimes:
# for tou in ToUTimes:
#     ToU = [SummerToU*tou, WinterToU*tou]
#     FiT = [FiTlowPV /fit, FiTmidPV/fit]
#     
#      
#     currentScen = surveyScenarios.Scenario(FiT, ToU, pvp, 500/bpt) #FiT, ToU, PVCapexDivider, BatteryPrice
#     #load = [0]*8760
#     payb = 0
#     discs = 0
#     for h in range(len(AllLoads)):
#         purchaseSavings = currentScen.purchaseSavings(feasibleConfigs[h][1], AllLoads[h], feasibleConfigs[h][2])
#         #purchaseSavings = currentScen.purchaseSavings(feasibleConfigs[h][1], load, feasibleConfigs[h][2])
#         payb +=purchaseSavings[1]
#         #print sum(PV3kW) *0.384*feasibleConfigs[h][1]/3
#         fullDisconnectionSavings = currentScen.fullDisconnectionSavings(AllLoads[h], feasibleConfigs[h][1], feasibleConfigs[h][2])
#         discs+=fullDisconnectionSavings[1]
#     payb/=len(AllLoads)    
#     discs/=len(AllLoads)
#     Paybacks.append(payb)
#     DSaved.append(discs)
#     #Base.append(500/bpt)
#     Base.append(tou)
# 
# #print Paybacks
# pl.plot(Base,Paybacks)
# pl.show()
# 
# pl.plot(Base,DSaved)
# pl.show()
SampleSystems = [[2, 0], [4,0], [6,0], [10,0], [2, 2], [4,3], [6,4], [10,7]]
#Options = ['A', 'B','C','D', 'E','F','G', 'H'] 
Options = ['A', 'B','C','D', 'A (with Battery)','B (with Battery)','C (with Battery)', 'D (with Battery)'] 
AvePaybacks = []
SystemCost = []
ROI = []
for system in SampleSystems:
    #for pvp in PVPriceTimes:
    ToU = [SummerToU, WinterToU]
    FiT = [FiTlowPV*1, FiTmidPV*1]
    
     
    currentScen = surveyScenarios.Scenario(FiT, ToU, 1, 760,[]) #FiT, ToU, PVCapexDivider, BatteryPrice
    #load = [0]*8760
    payb = 0
    discs = 0
    roi = 0
    #for h in range(len(AllLoads)):
    for meter in ResMeterID:
        load = GetMeterReadings(meter)
        purchaseSavings = currentScen.purchaseSavings(system[0], load, system[1])
        #purchaseSavings = currentScen.purchaseSavings(system[0], AllLoads[h], system[1])
        payb +=purchaseSavings[1]
        roi += purchaseSavings[2]
    SystemCost.append(purchaseSavings[0])   
    payb/=len(ResMeterID)
    roi/=len(ResMeterID)
    #payb/=len(AllLoads)
    AvePaybacks.append(payb)
    ROI.append(roi)
    #Base.append(500/bpt)
    #Base.append(tou)

pl.scatter(SystemCost, ROI)
pl.xlim(xmin=0)
pl.ylim(0,max(ROI)+2)
pl.title('PV-Battery Systems')
pl.xlabel('System Cost ($)', fontsize=18)
pl.ylabel('Annual Return on Investment (%)', fontsize=18)

# for label, x, y in zip(labels, data[:, 0], data[:, 1]):
#     plt.annotate(
#         label, 
#         xy = (x, y), xytext = (-20, 20),
#         textcoords = 'offset points', ha = 'right', va = 'bottom',
#         bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
#         arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
#     
for payback, cost, roi, option in zip(AvePaybacks, SystemCost, ROI, Options):
    pl.annotate('Payback: ' +str('%.0f' % payback)+ ' years', xy = (cost,roi),xytext = (10, 0),
                textcoords = 'offset points', ha = 'left', va = 'center',
                bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5))
    
    pl.annotate(option, xy = (cost,roi), xytext = (0, -25),
                textcoords = 'offset points', ha = 'left', va = 'bottom',
                #bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5)
                )
    print payback, cost, roi
pl.show()
