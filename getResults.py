#!/usr/bin/python
from __future__ import division 
import pickle
import numpy as np
import matplotlib.pyplot as plt
import math
import numpy as np
import pylab as ppl
import copy

def GetLoads(Load, period, ltype):
    if period == 'day':
        hrs=24
    elif period == 'week':
        hrs = 168
        
    DailyPeak = []
    PeakTimes = []
    for d in range(int(math.floor(Load.shape[0]/hrs))):
        if ltype == 'peak':
            val = np.max(Load[d*hrs: (d+1)*hrs-1])
            argval = np.argmax(Load[d*hrs: (d+1)*hrs-1])
        elif ltype == 'total':
            val = np.sum(Load[d*hrs: (d+1)*hrs-1])
            argval = np.argmax(Load[d*hrs: (d+1)*hrs-1])
        DailyPeak.append(val)
        if hrs ==168:
            PeakTimes.append(np.remainder(argval,24))
        elif hrs ==24:
            PeakTimes.append(argval)
    return DailyPeak, PeakTimes

NumHomes = 2616/100 #Scaling factor
AllRes = []
for i in range(1,101):
    Res = pickle.load(open('results/OntarioPV_single_alt'+str(i),'rb'))
    AllRes.append(Res)
    del Res
Scenarios =  ['Base Case',  'Reduced Battery Price','Less Reduced FiT','Increased ToU', 'Reduced PV Price', 'Reduced PV and Battery Price', 'Increased ToU and Reduced Battery Price', 'Increased ToU and Reduced PV Price']
ScenariosDict = {'base case': 0,  'reduced battery price':1,'less reduced fit':2,'increased tou':3, 'increased tou and reduced battery price':4}
PVNum = {'base case': [],'upfront subsidy':[], 'reduced tou':[], 'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[]}

NumberOfSims = 20
lines = [':','-','-.','--']
colors = ['k','b','r','g','c']

PVkW  = {'base case': [], 'upfront subsidy':[],'reduced tou':[], 'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[] , 'increased tou and reduced pv price':[]}
BattNum = {'base case': [], 'upfront subsidy':[], 'reduced tou':[],'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[] , 'increased tou and reduced pv price':[]}
BattkWh = {'base case': [], 'upfront subsidy':[],'reduced tou':[], 'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[] }
Loads = {'base case': [],  'upfront subsidy':[],'reduced tou':[],'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[] , 'increased tou and reduced pv price':[]}
Gen = {'base case': [], 'upfront subsidy':[], 'reduced tou':[],'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[] }
Paybacks = {'base case': [],'upfront subsidy':[],'reduced tou':[],  'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[] , 'increased tou and reduced pv price':[]}
TVars = []
count = 0
PVNumFiT = {'base case': [], 'upfront subsidy':[], 'reduced tou':[],'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[]}
PVNumNetM = {'base case': [], 'upfront subsidy':[],'reduced tou':[], 'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[]}
BattNumFiT = {'base case': [], 'upfront subsidy':[],'reduced tou':[], 'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[]}
BattNumNetM = {'base case': [], 'upfront subsidy':[],'reduced tou':[], 'reduced battery price':[],'less reduced fit':[],'increased tou':[], 'increased tou and reduced battery price':[], 'reduced pv price': [], 'reduced pv and battery price':[], 'increased tou and reduced pv price':[]}

#BaseCaseRes = dict()
with open('Base Case Ontario Results', 'rb') as f:
    BaseCaseRes = pickle.load(f)
    
for res in AllRes:
    s = res['name']
    PVNum[s].append(np.cumsum(res['PVNum'])/NumHomes)
    PVNumFiT[s].append(np.cumsum(res['PVNumFiT'])/NumHomes)
    PVNumNetM[s].append(np.cumsum(res['PVNumNetM'])/NumHomes)
#     plt.plot(np.cumsum(res['PVNumNetM']))
#     plt.plot(np.cumsum(res['PVNumFiT']))
#     plt.show()
    PVkW[s].append(np.cumsum(res['PVkW'])/NumHomes)
    BattNum[s].append(np.cumsum(res['BattNum'])/NumHomes)
    BattkWh[s].append(np.cumsum(res['BattkWh'])/NumHomes)
    Loads[s].append(res['Load']/NumHomes)
    Gen[s].append(res['Generation']/NumHomes)
    Paybacks[s].append(res['Paybacks'])
    
del AllRes  
#     plt.plot(res['Paybacks'])
#     plt.show()
# resnames = ['base case',  'reduced battery price','less reduced fit','increased tou', 'increased tou and reduced battery price', 'reduced pv price', 'reduced pv and battery price', 'increased tou and reduced pv price']
# for res in resnames:
#     for l in Loads[res]:
#         plt.plot(l, alpha=0.2)
#     plt.show()

def plotPVAdoption(scenar, systype):
    mainPV = []
    scen = scenar.lower()
    if systype in ['PV','Battery','PVkW','BattkWh', 'PVNetM', 'PVFiT', 'BattNetM', 'BattFiT']:
        
        if systype == 'PV':
            var = PVNum[scen]
        elif systype == 'Battery':
            var = BattNum[scen]
        elif systype == 'PVkW':
            var = PVkW[scen]
        elif systype == 'BattkWh':
            var =  BattkWh[scen]
        elif systype == 'PVNetM':
            var = PVNumNetM[scen]
        elif systype == 'PVFiT':
            var = PVNumFiT[scen]
        elif systype == 'BattNetM':
            var = BattNumNetM[scen]
        elif systype == 'BattFiT':
            var = BattNumFiT[scen]
        
            
        for idx, pvn in enumerate(var):
            if idx !=0:
                mainPV = np.column_stack((mainPV ,np.asarray(pvn)))
            else:
                mainPV = np.asarray(pvn)

        
     
    meanRes = np.mean(mainPV, axis =1)
    
    tval = 1.725#1.99 #1.987
    errRes = np.std(mainPV, axis =1)*tval/math.sqrt(NumberOfSims)
#     ppl.errorbar(np.arange(0, 11), meanRes,errRes)
    

    return meanRes, errRes

def plotPaybacks(scenar):
    var1 = Paybacks[scenar.lower()]
    simcount = 0
    for pvn in enumerate(var1):
        #print pvn[1][1:]
        if not np.isnan(pvn[1][1:]).any():
            if simcount == 0:
                pb = pvn[1][1:]
            else:
                pb += pvn[1][1:]
            simcount+=1
    return pb/simcount
    
XTick = ['Jan 2015', '', '', '', 'Jan 2017', '', '', '', 'Jan 2019', '', ''\
         , '','Jan 2021', '', '', '', 'Jan 2023', '', '', '', 'Jan 2025', '', 'Jan 2026' ]
PVcomp = ['Base Case']#, 'Upfront Subsidy']#'Reduced PV Price','Increased ToU', 'Reduced Battery Price', 'Less Reduced FiT']
#Compare PV Adoption in select scenarios
count=0
colorcount =0
 
 
 
for scen in PVcomp:
    meanVar, errVar = plotPVAdoption(scen, 'PV')
 
     
    imglbl = scen
    if imglbl.lower() == 'less reduced fit':
        imglbl = 'Slowly Reduced FiT'
    if imglbl.lower() == 'reduced tou':
        imglbl = 'Reduced Electricity Price'  
    if imglbl.lower() == 'base case':
        imglbl = 'Adopted Germany Policies'  
    ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl , linestyle = lines[count], linewidth = 8, color = colors[colorcount], alpha = 0.8, elinewidth = 2)

    count+=1
    colorcount+=1
    if count ==4:
        count = 0

ppl.errorbar(np.arange(0, 21), BaseCaseRes['PV Mean'],BaseCaseRes['PV Error'], label = 'Original Ontario Base Case' , linewidth = 8,color = 'r', alpha = 0.7, elinewidth = 2)
ppl.errorbar(np.arange(0, 21), BaseCaseRes['Subsidy PV Mean'],BaseCaseRes['Subsidy PV Error'], label = '30% Upfront Subsidy' , linewidth = 8,color = 'g', alpha = 0.7, elinewidth = 2, linestyle = '--')  
ppl.legend(loc=2, numpoints = 1, fontsize = 35)
ppl.xlabel('Time (6-month Epochs)', size = 40)
ppl.ylabel('% of Agents with PV Systems', size = 40)
ppl.xticks(np.arange(0,21), XTick, size = 30)
ppl.yticks(size = 30)
ppl.title('Total PV Adoption', size = 50)
ppl.show()
        
# count=0
# colorcount =0
# for scen in PVcomp:
#     meanVar, errVar = plotPVAdoption(scen, 'PVFiT')
#     imglbl = scen
#     if imglbl.lower() == 'less reduced fit':
#         imglbl = 'Slowly Reduced FiT'
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl, linestyle = lines[count], linewidth = 8, color = colors[colorcount], alpha = 0.8, elinewidth = 2 )
#     count+=1
#     colorcount+=1
#     if count ==4:
#         count = 0
#        
# ppl.title('FiT Contracts', size =50)
# ppl.legend(loc=2, numpoints = 1, fontsize = 35)
# ppl.xlabel('Time (6-month Epochs)', size = 40)
# ppl.ylabel('Agents with PV Systems', size = 40)
# ppl.xticks(np.arange(0,21), XTick, size = 30)
# ppl.yticks(size = 30)
# ppl.show()
#       
#       
# count=0
# colorcount =0
# for scen in PVcomp:
#     meanVar, errVar = plotPVAdoption(scen, 'PVNetM')
#     imglbl = scen
#     if imglbl.lower() == 'less reduced fit':
#         imglbl = 'Slowly Reduced FiT'
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl , linestyle = lines[count], linewidth = 8, color = colors[colorcount], alpha = 0.8, elinewidth = 2)
#     count+=1
#     colorcount+=1
#     if count ==4:
#         count = 0
#        
# ppl.title('Net Metering Contracts', size =50)
# ppl.legend(loc=2, numpoints = 1, fontsize = 35)
# ppl.xlabel('Time (6-month Epochs)', size = 40)
# ppl.ylabel('Agents with PV Systems', size = 40)
# ppl.xticks(np.arange(0,21), XTick, size = 30)
# ppl.yticks(size = 30)
# ppl.ylim(0,)
# ppl.show()
       
# count=0
# colorcount =0
# for scen in Scenarios:#PVcomp:
#     meanVar, errVar = plotPVAdoption(scen, 'PVkW')
#     imglbl = scen
#     if imglbl.lower() == 'less reduced fit':
#         imglbl = 'Slowly Reduced FiT'
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl )
# ppl.legend(loc=2, numpoints = 1)
# ppl.xlabel('Month/Year', size = 17)
# ppl.ylabel('Total PV kW', size = 17)
# ppl.xticks(np.arange(0,21), XTick, size = '13')
# ppl.show()
        
        
#Compare Battery Adoption in all scenarios
SingleScenarios = ['Base Case']#, 'Upfront Subsidy']#'Less Reduced FiT', 'Reduced Battery Price','Increased ToU', 'Reduced PV Price', ]
MultipleScenarios = ['Base Case', 'Reduced PV and Battery Price', 'Increased ToU and Reduced Battery Price', 'Increased ToU and Reduced PV Price']
         
count=0
colorcount =0
for scen in SingleScenarios:
    meanVar, errVar = plotPVAdoption(scen, 'Battery')
 
     
    imglbl = scen
    if imglbl.lower() == 'base case':
        imglbl = 'Adopted Germany Policies' 
    if imglbl.lower() == 'less reduced fit':
        imglbl = 'Slowly Reduced FiT'
    ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl, linestyle = lines[count], linewidth = 8, color = colors[colorcount], alpha = 0.8, elinewidth = 2)
    count+=1
    colorcount+=1
    if count ==4:
        count = 0
        

ppl.errorbar(np.arange(0, 21), BaseCaseRes['Battery Mean'],BaseCaseRes['Battery Error'], label = 'Original Ontario Base Case' , linewidth = 8,color = 'r', alpha = 0.7, elinewidth = 2, linestyle = '-.')       
ppl.errorbar(np.arange(0, 21), BaseCaseRes['Subsidy Battery Mean'],BaseCaseRes['Subsidy Battery Error'], label = '30% Upfront Subsidy' , linewidth = 8,color = 'g', alpha = 0.7, elinewidth = 2, linestyle = '--')       
ppl.legend(loc=2, numpoints = 1, fontsize = 35)
ppl.xlabel('Time (6-month Epochs)', size = 40)
ppl.ylabel('% Agents with Battery Systems', size = 40)
ppl.xticks(np.arange(0,21), XTick, size = 30)
ppl.yticks(size = 30)
ppl.title('Total Battery Adoption', size = 50)
ppl.show()
 # count=0
# colorcount =0
# for scen in MultipleScenarios:
#     meanVar, errVar = plotPVAdoption(scen, 'Battery')
#     imglbl = scen
#     if imglbl.lower() == 'less reduced fit':
#         imglbl = 'Slowly Reduced FiT'
#          
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl, linestyle = lines[count], linewidth = 8, color = colors[colorcount], alpha = 0.8, elinewidth = 2)
#     count+=1
#     colorcount+=1
#     if count ==4:
#         count = 0
# ppl.legend(loc=2, numpoints = 1, fontsize = 35)
# ppl.xlabel('Time (6-month Epochs)', size = 40)
# ppl.ylabel('Agents with Battery Systems', size = 40)
# ppl.xticks(np.arange(0,21), XTick, size = 30)
# ppl.yticks(size = 30)
# ppl.show()
     
# for scen in Scenarios:
#     meanVar, errVar = plotPVAdoption(scen, 'BattFiT')
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl )
# ppl.legend(loc=2, numpoints = 1)
# ppl.xlabel('Month/Year', size = 17)
# ppl.title('FiT Contracts', size =20)
# ppl.ylabel('Agents with Batteries', size = 17)
# ppl.xticks(np.arange(0,21), XTick, size = '13')
# ppl.show()
#  
# for scen in Scenarios:
#     meanVar, errVar = plotPVAdoption(scen, 'BattNetM')
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl )
# ppl.legend(loc=2, numpoints = 1)
# ppl.title('Net Metering Contracts', size =20)
# ppl.xlabel('Month/Year', size = 17)
# ppl.ylabel('Agents with Batteries', size = 17)
# ppl.xticks(np.arange(0,21), XTick, size = '13')
# ppl.ylim(0,)
# ppl.show()
#     
    
# #Compare Battery Adoption in all scenarios
# for scen in Scenarios:
#     meanVar, errVar = plotPVAdoption(scen, 'BattkWh')
#     imglbl = scen
#     if imglbl.lower() == 'less reduced fit':
#         imglbl = 'Slowly Reduced FiT'
#     ppl.errorbar(np.arange(0, 21), meanVar,errVar, label = imglbl)
#       
# ppl.xlabel('Month/Year', size = 17)
# ppl.ylabel('Total Battery kWh', size = 17)
# ppl.xticks(np.arange(0,21), XTick, size = '13')
# ppl.legend(loc=2, numpoints = 1)
# ppl.show()
#     
#     
#Compare Paybacks in all scenarios
count = 0
colorcount = 0
for scen in PVcomp:
    meanVar= plotPaybacks(scen)
        
    imglbl = scen
    if imglbl.lower() == 'less reduced fit':
        imglbl = 'Slowly Reduced FiT'
    ppl.plot(meanVar, label = imglbl, linestyle = lines[count], linewidth = 8, color = colors[colorcount], alpha = 0.8,)

    count+=1
    colorcount+=1
    if count ==4:
        count = 0
ppl.plot(BaseCaseRes['Payback'], label = 'Original Ontario Base Case', linewidth = 8, color = 'r', alpha = 0.8,)
ppl.legend(loc=3, numpoints = 1, fontsize = 35)
ppl.xlabel('Time (6-month Epochs)', size = 40)
ppl.ylabel('Payback (Years)', size = 40)
ppl.xticks(np.arange(0,21), XTick, size = 30)
ppl.yticks(size = 30)
ppl.title('Payback Period', size = 50)
ppl.show()

#BaseCaseRes['Payback'] = meanVar
pickle.dump( BaseCaseRes, open( "Base Case Ontario Results", "wb" ) ) 

    

    
    
  
# 
# BattScenarios =  [ 'Base Case','Increased ToU', 'Reduced Battery Price', 'Reduced PV and Battery Price']
# bins = np.linspace(0,24,96)
# count = 0
# 
# #common_params = dict(bins=96, range=(0, 24), normed=True)
# 
# AllPeaks = []
# for idx2, scen in enumerate(Scenarios):
#     for idx, load in enumerate(Loads[scen.lower()]):
#         gen = Gen[scen.lower()][idx]
#         AllLoad = np.array([])
#         #netLoad = Loads[scen][idx]
#          
#         peak, times = GetLoads(load,'week','total')
#         genpeak, peaktimes = GetLoads(gen,'day','peak')
#         if idx:
#             allPeaks +=np.asarray(peak)# np.column_stack((allPeaks ,peak ))
#             allGen +=np.asarray(genpeak)
#             PeakTimes += times
#         else:
#             allPeaks = np.asarray(peak)#-np.asarray(genpeak)
#             allGen = np.asarray(genpeak)
#             PeakTimes = times
#         
#     if scen == 'Base Case':
#         basePeak = allPeaks
#     AllPeaks.append(PeakTimes)
#     count+=1
#     
#     print allPeaks.shape
#     avePeak = allPeaks/NumberOfSims/1000
#     plt.plot(avePeak.astype(int),  alpha = 1,linestyle = '-', linewidth = 2,label = scen)
#     
#     #break
# #plt.legend(loc =2, fontsize = 30)
# plt.title('Weekly Peak Electricity',  size = 50)
# plt.xlabel('Time (Week)', size = 40)
# plt.ylabel('Electricity Load (MW)',size = 40)
# plt.xticks(size = 30)
# plt.yticks(size = 30)
# plt.ylim(0,)
# #plt.xlim(0,550)
# plt.show()
# 
# plt.hist(AllPeaks,24, normed=1, histtype='bar', label=Scenarios)
# plt.legend(loc=2, fontsize = 30)
# plt.title('Peak Load Hours',  size = 40)
# plt.xlabel('Time Of Day', size = 30)
# plt.ylabel('Peak Occurence Frequency',size = 30)
# plt.xticks(size = 25)
# plt.yticks(size = 25)
# plt.show()
# count = 0
# for scen in PVcomp:
#     for idx, gen in enumerate(Gen[scen.lower()]):
#         AllGenP = []
#         netLoad = gen
#           
#         peak, peaktimes = GetLoads(netLoad,'week','total')
#         if idx:
#             allPeaks += np.asarray(peak)# np.column_stack((allPeaks ,peak ))
#             PeakTimes += peaktimes
#         else:
#             allPeaks = np.asarray(peak)
#             PeakTimes = peaktimes
#     plt.plot(allPeaks/NumberOfSims/1000, label = scen, linestyle = lines[count], alpha = 1, linewidth = 4)
#     count+=1
#     AllGenP.append(PeakTimes) 
#     if count ==4:
#         count = 0
#     
# plt.legend(loc =2, fontsize = 35)
# plt.title('Total Weekly PV Generation',  size = 50)
# plt.xlabel('Time (Week)', size = 40)
# plt.ylabel('PV Electricity Generated (MWh)',size = 40)
# plt.xticks(size = 30)
# plt.yticks(size = 30)
# plt.ylim(0,)
# plt.xlim(0,550)
# plt.show()
# 
# plt.hist(AllGenP,24, normed=1, histtype='bar', label=Scenarios)
# plt.legend(loc=2, fontsize = 30)
# plt.title('Peak Load Hours',  size = 40)
# plt.xlabel('Time Of Day', size = 30)
# plt.ylabel('Peak Occurence Frequency',size = 30)
# plt.xticks(size = 25)
# plt.yticks(size = 25)
# plt.show()
#         #plt.show()
#         
# #     for pvn in Loads[scen]:
# # 
# #         print scen, np.mean(pvn)
# #         dPeak = PeakLoads(pvn)
# #         plt.plot(pvn,linestyle = '--', alpha=0.7)
# # #     for pvn in PVNum[scen]:
# # #         plt.plot(pvn)
# #     plt.show()
# 
# x=1
# 
# 
#     