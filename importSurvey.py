from __future__ import division
import csv
import numpy as np
#import statsmodels.api as sm
import pandas as pd
from statsmodels.formula.api import logit
from deflection import newDeflection

from sklearn.linear_model import Ridge
from sklearn.metrics import r2_score

import matplotlib.pyplot as plt
import pylab as pl
#from mpl_toolkits.mplot3d import Axes3D
#import random
from matplotlib.mlab import PCA
import math

from sklearn.linear_model import RandomizedLogisticRegression, LinearRegression, lars_path, LassoLarsCV, LassoLars, LogisticRegression
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.preprocessing import normalize
import copy



#----------------------------#
#Code from https://gist.github.com/perrygeo/4512375
def scale_linear_bycolumn(rawpoints, high=100.0, low=0.0):
    mins = np.min(rawpoints, axis=0)
    maxs = np.max(rawpoints, axis=0)
    rng = maxs - mins
    return high - (((high - low) * (maxs - rawpoints)) / rng)

#End
#------------------------------#


#List of PV-Battery Options
# Payback, System COst, Annual ROI, Battery (1 without, 2with)

PVBattSystems = [(15, 12460, 1.8, 1), #A
                 (10, 16960, 5.1, 1), #B
                 (8, 21000, 7.2, 1), #C
                 (18, 27800, 0.7, 2), #C with battery
                 (7, 31400, 8.6, 1), #D
                 (17, 43300, 0.8, 2), #D with battery
                 
                 (11,   9345,    4.1, 1),#A
                 (14,    10345,    2.1, 2),#A with battery
                 (7,    12720,    8.4, 1),#B
                 (9,    14220,    5.7, 2),#B with battery
                 (6,    15750,    11.2, 1),#C
                 (8,    17750,    8.0, 2),#C with battery
                 (6,    23550,    13.1, 1),#D
                 (7,    27050,    9.0, 2),#D with battery
                 
                 (15, 9345,    1.8, 1),#A
                 (10, 12720,    5.1, 2),#A with battery
                 (8, 15750,    7.2, 1),#B
                 (7, 23550 ,   8.6, 2),#B with battery
                 (16, 9745 ,   1.2, 1),#C
                 (11, 13320,    4.2, 2),#C with battery
                 (9, 16550,    6.1, 1),#D
                 ( 8, 24950,    7.2, 2),#D with battery

                                  ]

crowd_file = open("survey/crowdflow_.csv", "rb")
crowd_reader = csv.reader(crowd_file)

ip_adds = []
Ontario = 0
for crowd in crowd_reader:
    if crowd[9]!= 'ON':
        ip_adds.append(crowd[11])
        Ontario +=1 
crowd_file.close()

survey_file = open("survey/solar_batt_.csv", "rb")
survey_reader = csv.reader(survey_file)
surveyresp= []
validResponses = 0

Respondents = []
Indep = []
PurchaseChoices = []

EPAList = [[],[],[],[],[]]
SocialInfluence = []
Fakes = 0
Real = 0
for res in survey_reader:
    #Test Question 
    EPA= False
    #print res[36]
    if res[38] =='3':
        Real+=1
    else:
        Fakes +=1
    if res[38] == '3' and res[4] not in ip_adds:# and (res[24] =='1' or res[24]=='2'):
        purcFrac = 0
        #How much do you know about solar panels
        #print res[10]
        
        #Do you currently have solar panels installed at your home/business?
        #print res[11]
        
        #Buying systems
        
        #print res[12:33]

        #Sentiments
        #print res[39]
        
        #Remove bad responses, i.e., click-through responses
        setBuy = res[12]
        goodResponse =  False
        for k in range(1,len(PVBattSystems)):
            if res[12] != res[12+k]:
                goodResponse = True
                break
        #Only Yes responses
        yesOnlyResp = 0
        for k in range(0,len(PVBattSystems)):
            if res[12+k] == '' or res[12+k] == '1':
                yesOnlyResp = 1
            else:
                yesOnlyResp = 0
                break
            
        if yesOnlyResp:
            for k in range(0,len(PVBattSystems)):
                if res[12+k] == '':
                    res[12+k] = '2'
        
        setEPA = res[40]
        goodEPA = False
        for i in range(40,55):
            if res[i]:
                EPA =  True
                if setEPA != res[i]:
                    goodEPA = True
            else:
                EPA = False
                break
            
        goodE =  False
        goodP =  False
        goodA =  False
        
        if EPA:
            setE = res[40]
            for k in range(40,45):
                if res[k] != setE:
                    goodE= True
                    break
            
            setP = res[45]
            for k in range(45,50):
                if res[k] != setP:
                    goodP= True
                    break
                    
            setA = res[50]
            for k in range(50,55):
                if res[k] != setA:
                    goodA= True
                    break
                    
        #print res[40:55]
        if goodResponse and EPA and goodEPA and goodE and goodP and goodA:
#             HomeOwnerEPA = ([int(res[40]), int(res[45]), int(res[50])])
#             BusOwnerEPA =([int(res[41]), int(res[46]), int(res[51])])
#             BuyingEPA = ([int(res[42]), int(res[47]), int(res[52])])
#             SolarPanelEPA= ([int(res[43]), int(res[48]), int(res[53])])
#             BatteryEPA= ([int(res[44]), int(res[49]), int(res[54])])
#             
#             EPAList[0].append([int(res[40]), int(res[45]), int(res[50])])
#             EPAList[1].append([int(res[41]), int(res[46]), int(res[51])])
#             EPAList[2].append([int(res[42]), int(res[47]), int(res[52])])
#             EPAList[3].append([int(res[43]), int(res[48]), int(res[53])])
#             EPAList[4].append([int(res[44]), int(res[49]), int(res[54])])
              
 
            D1 = 1#newDeflection(HomeOwnerEPA, BuyingEPA, SolarPanelEPA)
            D2 = 1#newDeflection(HomeOwnerEPA, BuyingEPA, BatteryEPA)

            #print D
            if res[10] and res[39] and res[37] and res[36] and res[35] and res[11]:
                validResponses+=1
                SocialInfluence.append(int(res[35]))

                Respondents.append((D1, D2, int(res[39]), int(res[37]), int(res[36]), int(res[10]), int(res[11]), int(res[35])))#, purcFrac #   , int(res[36])))#, 1))

                for j in range(0,len(PVBattSystems)):
                    
                    if res[12+j]:
                        if int(res[12+j]) == 1:
                            purcFrac +=1
#                             PurchaseChoices.append(0) #1 = Yes, 2= No
#                         else:
#                             PurchaseChoices.append(1)
                        
                        Indep.append((int(res[12+j])-1,) +  PVBattSystems[j] \
                                     + (D1, D2, int(res[39]), int(res[37]), int(res[36]), int(res[10]), int(res[11]), int(res[35])))#, purcFrac #   , int(res[36])))#, 1))

# homeowners = np.mean(np.asarray(EPAList[0]), axis=0)
# busowners = np.mean(np.asarray(EPAList[1]), axis=0)
# buying = np.mean(np.asarray(EPAList[2]), axis=0)
# solarpanels = np.mean(np.asarray(EPAList[3]), axis=0)
# batts = np.mean(np.asarray(EPAList[4]), axis=0)
#     if validResponses >5:
#         break
# for i in range(0,len(Indep)):
#     homeepa = getEPA(homeowners)
#     solarpanelepa = getEPA(solarpanels)
#     buyingepa = getEPA(buying)
#       
#     D1 = newDeflection(homeepa, buyingepa, solarpanelepa)
#      
#      
#     Indep[i] +=  (D1,)


print 'real,fake', Real, Fakes       
#print 'Valid Responses= ', validResponses
#print len(PurchaseChoices), len(Indep), len(EPAList[0])

survey_file.close()

# print np.asarray(Indep)
# plt.hist(SocialInfluence)
# plt.show()

pd.options.display.float_format = '${:,.2f}'.format
SurveyFrame = pd.DataFrame(np.array(Indep), columns=['Decision', 'Payback','Cost','ROI', 'Battery', 'PVDeflection','BattDeflection','MaxPay','Env2','Env1','SolarKnow', 'SolarInstalled', 'SocialInf'])
# plt.hist(SurveyFrame['Payback'], SurveyFrame['MaxPay'])
# plt.show()
 
buy_logit = logit(formula = 'Decision ~  Payback + Battery  +   Cost + MaxPay ', data=SurveyFrame).fit()
print buy_logit.summary()

#Check residuals
idx = np.argsort(np.asarray(buy_logit.fittedvalues))
argGen = np.asarray(buy_logit.resid_generalized)[idx]
sortedFit = np.sort(np.asarray(buy_logit.fittedvalues))
minFit = sortedFit[0]
maxFit = sortedFit[-1]
print minFit, maxFit
# plt.plot(sortedFit)
# plt.show()
NumBins = 40
binStep = (maxFit-minFit)/NumBins

Fitted = np.zeros(NumBins+1)
General = np.zeros(NumBins+1)
BinCount = np.zeros(NumBins+1)

for idx, fit  in enumerate(sortedFit):
   
    curbin = int(math.floor((fit - minFit)/binStep))
    Fitted[curbin]+= fit
    General[curbin] += argGen[idx]
    BinCount[curbin] +=1

ConfInts = 2* np.sqrt(.95*0.5/BinCount)

#plt.scatter(buy_logit.fittedvalues, buy_logit.resid_generalized)
plt.scatter(Fitted/BinCount, General/BinCount, s = 25)
plt.plot(Fitted/BinCount, ConfInts, linestyle = '--',label = 'Upper 95% Confidence Interval')
plt.plot(Fitted/BinCount, -ConfInts, linestyle = '--',label = 'Lower 95% Confidence Interval')
plt.scatter(Fitted/BinCount, ConfInts,color = 'r', alpha = 0.5)
plt.scatter(Fitted/BinCount, -ConfInts,color ='r', alpha = 0.5)
# plt.plot(np.zeros(2)-0.137)
plt.xlabel('Average Fitted Values')
plt.ylabel('Average Residuals')
plt.legend()
plt.show()

indepArray = np.array(Indep)
np.random.shuffle(indepArray)


# plt.plot(np.sort(np.absolute(np.sum(result.Y, axis = 0)*10**12)))
# plt.show()
# testX = result.project(indepArray[25,1:], minfrac=0.05)
# testY = result.center(indepArray[25,1:])
# print testX, testY
# for a in range(0,10):
#     np.random.shuffle(indepArray)
#     trainX = indepArray[1:1800,1:]
#     trainY = indepArray[1:1800,0]
#     ridge = Ridge(alpha=10)
#     ridge.fit(trainX, trainY)
#     plt.plot(abs(ridge.coef_))
# plt.xlabel('Features')
# plt.ylabel('Coefficients')
# plt.title('alpha = 10')
# plt.show()
#print "Ridge model:", ridge.coef_
# print

# rand_log = RandomizedLogisticRegression()
# trainedlog = rand_log.fit(trainX, trainY)
# print rand_log.get_params()

# lr = LinearRegression()
# lr.fit(trainX,trainY)
# print "Linear model:", lr.coef_
# plt.plot(abs(ridge.coef_))
# plt.xlabel('Features')
# plt.ylabel('Coefficients')
# #plt.xticks(['Payback','Cost','ROI', 'Battery', 'PVDeflection','BattDeflection','MaxPay','Env2','Env1','SolarKnow', 'PVDef:SolarKnow', 'BattDef:Batt'])
# plt.show()

# 
# clf = ExtraTreesClassifier()
# X_new = clf.fit(trainX, trainY).transform(trainX)
# print clf.feature_importances_  
# 




splits=[]
for a in range(0,10):
    splits.append(indepArray[a*456:(a+1)*456,:])
Coeff = np.zeros(14)
   
predictionSetPoint = 0.5
# for c in range(0,10):
#     tempSplit = copy.copy(splits)
#     test = tempSplit.pop(c)
#     tr = 0
#     for ts in tempSplit:
#         if tr != 0: 
#             train = np.vstack((train, ts))
#         else:
#             train = ts
#             tr = 1
# #     #train = np.array(tempSplit)
# #     SurveyFrame = pd.DataFrame(np.array(train), columns=['Decision', 'Payback','Cost','ROI', 'Battery', 'PVDeflection','BattDeflection','MaxPay','Env2','Env1','SolarKnow','SolarInstalled','SocialInf','1A','2A'])
# #     buy_logit = logit(formula = 'Decision ~  Payback  + Battery  + MaxPay + Cost', data=SurveyFrame).fit( disp = False)
# #        
# #     #Cross validation of prediction
# #     truePred = 0
# #     falsePred = 0
# #     for y in test:
# #         prediction = buy_logit.predict(exog=dict(Payback= y[1], Battery = y[4], Cost = y[2], MaxPay = y[7], PVDeflection = y[5], SolarKnow = y[10], Env2 = y[8]))
# #         if prediction > predictionSetPoint:
# #             prediction = 1
# #         else:
# #             prediction = 0
# #            
# #         if prediction == y[0]:
# #             truePred +=1
# #         else:
# #             falsePred +=1
#        
#        
#     print buy_logit.summary()
#     trainX = normalize(train[:,1:])
#     trainY = train[:,0]
#     #ridge = LogisticRegression(penalty='l2')
#     ridge = LassoLars(alpha=0.000055)
# #     row_sums = trainX_.sum(axis=1)
# #     trainX = trainX_ / row_sums[:, np.newaxis]
#     ridge.fit(trainX, trainY)
# #     print "Ridge model:", ridge.coef_
#     Coeff  = Coeff + abs(ridge.coef_)
#     #print ridge.coef_
# #     plt.plot(abs(ridge.coef_))
# #        
# #     #print 'Accuracy= ', truePred/(truePred+falsePred)
# # plt.xlabel('Features')
# # plt.ylabel('Coefficients')
# # plt.title('alpha = 5.5e-07')
# # plt.show()
# # #        
# plt.plot(Coeff/10)
# plt.xlabel('Features',size=17)
# plt.ylabel('Coefficients',size = 17)
# plt.title('alpha = 5.5e-07', size = 20)
# plt.show()
# 
# 
# alphas, _, coefs = lars_path(trainX, trainY, method='lasso', verbose=True)
# 
# xx = np.sum(np.abs(coefs.T), axis=1)
# xx /= xx[-1]
# labels = ['Payback','Cost','ROI', 'Battery', 'PVDeflection','BattDeflection','MaxPay','Env2','Env1','SolarKnow', 'PVDef:SolarKnow', 'BattDef:Batt']
# plt.plot(xx, coefs.T, label = labels)
# print alphas
# print coefs
# ymin, ymax = plt.ylim()
# plt.vlines(xx, ymin, ymax, linestyle='dashed')
# plt.xlabel('|coef| / max|coef|')
# plt.ylabel('Coefficients')
# plt.title('LASSO Path')
# plt.axis('tight')
# plt.show()

# trainX = indepArray[:,1:]
# trainY = indepArray[:,0]
# model = LassoLarsCV(cv=20).fit(trainX, trainY)
# 
# 
# # Display results
# m_log_alphas = -np.log10(model.cv_alphas_)
# 
# pl.figure()
# pl.plot(m_log_alphas, model.cv_mse_path_, ':')
# pl.plot(m_log_alphas, model.cv_mse_path_.mean(axis=-1), 'k',
#         label='Average across the folds', linewidth=2)
# pl.axvline(-np.log10(model.alpha), linestyle='--', color='k',
#            label='alpha CV')
# pl.legend()
#  
# pl.xlabel('-log(lambda)')
# pl.ylabel('Mean square error')
# pl.title('Mean square error on each fold: Lars ')
# pl.axis('tight')
# #pl.ylim(ymin, ymax)
#  
# pl.show()                                    
# 
# pl.plot(abs(model.coef_))
# pl.xlabel('Features')
# pl.ylabel('Coefficients')
# pl.show()
# 
# print model.alpha