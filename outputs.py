from __future__ import division

# import matplotlib.pyplot as plt
import numpy as np
from griddefection import *
from importSurvey import Respondents
import random
import pvbattsys
import pickle
import owners

def init(TVars, NumberOfHomeOwners, agentreplicas,simmode):
    global HomeOwnerList, CurrentDate, PVAdoptionNum, PVAdoptionkW, BatteryAdoptionkWh, BatteryAdoptionNum, SystemPaybacks, PurchaseAtt, Adoption
    global NetLoadOnGrid 
    PVAdoptionkW = np.zeros( SimEndDate+1) #PV Adoption in KW    
    PVAdoptionNum = np.zeros( SimEndDate+1) #PV Adoption in number of installed systems
    SystemPaybacks = np.zeros( SimEndDate+1)
    PurchaseAtt = np.zeros( SimEndDate+1)
    BatteryAdoptionkWh = np.zeros(SimEndDate+1) #in kWh
    BatteryAdoptionNum = np.zeros(SimEndDate+1) 
    CurrentDate = 0
    SocialSizes = []
    Adoption = np.zeros( SimEndDate+1)
    NetLoadOnGrid = np.array([])#np.zeros(int(SimEndDate*Epoch/12* 8760))
    HomeOwnerList = []
    AllIDs= np.arange(0, NumberOfHomeOwners*agentreplicas)
    #Create Scenario
     
    #Create list of potential scenario variables 
    #Collect load data from file
    with open('AllLoads2', 'rb') as f:
        AllLoads = pickle.load(f)
    ID=0
    withPV=0
    random.shuffle(Respondents)
    maxPVowners = math.ceil(NumberOfHomeOwners*agentreplicas *0.004) #0.037 for Passau
    for i in range(0, NumberOfHomeOwners):
        k =0
        agentUnassigned = 1
        while k < agentreplicas:
            surveyVars = Respondents[i]# Indep[i]
            newPVSys = 0
            if simmode == 'main':
                if surveyVars[6] == 1 and agentUnassigned and maxPVowners:
                    agentUnassigned = 0
                    maxPVowners -=1
                    withPV+=1
                    newPVSys=pvbattsys.PVBattSystem(3, 0, CurrentDate, 'FiT')
                
        
#                 PVAdoptionNum[0] +=1
#                 PVAdoptionkW[0] += 3
                
            PotentialFriends = np.select([AllIDs!=ID], [AllIDs])
            numFriends = int(math.ceil(random.betavariate(1,5) *10))

            socNet = random.sample(PotentialFriends, numFriends)
            
            load = random.choice(AllLoads)
            socialT = random.normalvariate(TVars[0],TVars[1])#random.betavariate(1.5,2)
            ho = owners.HomeOwner(ID, surveyVars, newPVSys, load, socNet, socialT) #How many agents already have PV systems installed?
            HomeOwnerList.append(ho)
            ID+=1
            k+=1
    

#     for ho in HomeOwnerList:
#         PotentialFriends = np.select([AllIDs!=ho.ID], [AllIDs])
#         numFriends = int(math.ceil(random.betavariate(1,5) *8))
#         socNet = random.sample(PotentialFriends, numFriends)
#         ho.network += socNet
#         for soc in socNet:
#             HomeOwnerList[soc].network.append(ho.ID)
# 
#     for ho in HomeOwnerList:
#         SocialSizes.append(len(ho.network))
        
#     plt.hist(SocialSizes)
#     plt.show()
    print 'Homes with PV', withPV
    return HomeOwnerList, PVAdoptionNum, PVAdoptionkW