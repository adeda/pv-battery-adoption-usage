from __future__ import division
import surveyScenarios
from griddefection import *#FiTlowPV, FiTmidPV, SummerToU, WinterToU
import pickle
import numpy as np
import csv
import pylab as pl


#Collect load data from file
with open('AllLoads', 'rb') as f:
    AllLoads = pickle.load(f)
#print len(AllLoads)  
with open('feasibleConfigs', 'rb') as f:
    feasibleConfigs = pickle.load(f)


# with open("sampleload.csv", "wb") as f:
#     writer = csv.writer(f)
#     writer.writerow(np.transpose(np.asarray(AllLoads[1])))                     
  
ToUTimes= np.linspace(1,4,7)
FiTTimes = np.linspace(1/10,1,10)
PVPriceTimes = np.linspace(1/4,1,6)
BattPriceTimes = np.linspace(1/10,1,10)

fit=1
pvp=1
bpt =1
tou =1
 


for idx, config in enumerate(feasibleConfigs):
    # 
    # print 1500* BattPriceTimes[0]
    Paybacks= []
    DSaved = []
    
    #for bpt in BattPriceTimes:

    #for tou in ToUTimes:
    for pvp in PVPriceTimes:
    #for fit in FiTTimes:
        ToU = [SummerToU*tou, WinterToU*tou]
        FiT = [FiTlowPV *fit, FiTmidPV*fit]
        currentScen = surveyScenarios.Scenario(FiT, ToU, pvp, 1500*bpt,[]) #FiT, ToU, PVCapexDivider, BatteryPrice
        payb = 0
        discs = 0
        #for h in range(len(AllLoads)):
        purchaseSavings = currentScen.purchaseSavings(config[1], AllLoads[1], config[2])
        payb +=purchaseSavings[1]
        fullDisconnectionSavings = currentScen.fullDisconnectionSavings(AllLoads[1], config[1], config[2])
        discs+=fullDisconnectionSavings[1]
        #print feasibleConfigs[1]
         
    #     payb/=len(AllLoads)    
    #     discs/=len(AllLoads)
     
        Paybacks.append(payb)
        DSaved.append(discs)
        
    #     Base.append(pvp)
      
    #print Paybacks
    # print Base
    # print Paybacks
    #print DSaved
    if idx == 0:
        Losses = -1* np.asarray(DSaved)
    else:
        Losses = Losses - np.asarray(DSaved)
Base = PVPriceTimes

Losses/=len(feasibleConfigs)    
print len(feasibleConfigs)
pl.plot(Base,Losses,color = 'r',linestyle= '--',alpha=0.8)
pl.scatter(Base,Losses)
 
# pl.plot([0,500],[0,0], 'r--')
# pl.plot([0,500],[20,20], 'r--')
pl.xlim(xmin=0)
pl.ylim(ymin=0)
ax = pl.gca()
ax.get_xaxis().get_major_formatter().set_scientific(False)
pl.title('Off-Grid Operation', size = '22')
pl.ylabel('Annual Losses ($)', size = '17')
pl.xlabel('PV Price Multiplier', size = '17')
pl.show()
  
pl.scatter(Base,DSaved)
ax = pl.gca()
ax.get_xaxis().get_major_formatter().set_useOffset(False)
pl.ylabel('Disconnection Savings ($)')
pl.xlabel('ToU Scaling Factor')
pl.show()
 

