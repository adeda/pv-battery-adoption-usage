from __future__ import division
from griddefection import *
import surveyScenarios
import owners
import outputs
from importSurvey import Indep, Respondents
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import pickle
import random
import csv
import pvbattsys
import copy
import math

#Initialize Agents and Environmental Vars
NumberOfHomeOwners = len(Respondents)
  
ElectricityGeneration = np.zeros(8760 * SimEndDate)
  
  
#Changes: number of replicas, scenarios, threshold
def getPriceDecay(y,x):
      
    prices = np.linspace(y,y, SimEndDate+1)
    decay = []
      
    decayfactor = 10** (math.log10(x/y)/(5))
    for i in range(SimEndDate+1):
        decay.append(round(prices[i]* decayfactor**i,2))
      
    return np.asarray(decay)
      
      
SocialSizes = []
agentreplicas = 1
AllHomeOwnerIDs = np.linspace(0, (NumberOfHomeOwners-1)*agentreplicas, (NumberOfHomeOwners-1)*agentreplicas)
AllHomeOwnerIDs = AllHomeOwnerIDs.astype(int)
TVars_ = [0.4,0.13]
  
#Create price decays
pvprice_base = getPriceDecay(1,0.58)
# plt.plot(pvprice_base)
# plt.show()
pvprice_2 = getPriceDecay(1,0.29)
battprice_base = getPriceDecay(1,0.5)
battprice_2 = getPriceDecay(1,0.25)
  
# plt.plot(battprice_2)
# plt.plot(pvprice_2)
# plt.show()
ScenVars_ = [{'name': 'base case', 'BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
            'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
                           
   
#                
#               {'name': 'reduced battery price','BattPrice': battprice_2, 'FiT': np.linspace(1,0,SimEndDate+1),\
#              'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
#                  
#              {'name': 'less reduced fit','BattPrice': battprice_base, 'FiT': np.linspace(1,0.3,SimEndDate+1),\
#             'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
#                  
#              {'name': 'increased tou','BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
#              'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,6,SimEndDate+1)},
#                 
#                              
#              {'name': 'increased tou and reduced battery price','BattPrice': battprice_2, 'FiT': np.linspace(1,0,SimEndDate+1),\
#              'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,6,SimEndDate+1)},
#                 
#             {'name': 'reduced pv price','BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
#             'PVPrice' :pvprice_2,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
#        
#               {'name': 'reduced pv and battery price','BattPrice': battprice_2, 'FiT': np.linspace(1,0,SimEndDate+1),\
#              'PVPrice' :pvprice_2,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
               
             ]
  
with open('AllLoads2', 'rb') as f:
    AllLoads = pickle.load(f)
ID=0
withPV=0
HomeOwnerList = []
PVs = [3,6,10]
Batts = [0]
surveyVars = []
for load in AllLoads:
    #newPVSys=pvbattsys.PVBattSystem(3, 0, CurrentDate)
    ho = owners.HomeOwner(ID, surveyVars, 0, load, [], 0) #How many agents already have PV systems installed?
    HomeOwnerList.append(ho)
    if len(HomeOwnerList)>5:
        break
  
  
#Run simulation years
BattCost = 1500
AllScens = []
AllScensNM = []
for sv in ScenVars_:
    Updater= {'FiT': sv['FiT'].tolist(), 'ToU': sv['ToU'].tolist(), 'Battery': sv['BattPrice'].tolist(), 'PV': sv['PVPrice'].tolist()}
      
    Paybacks = np.zeros( SimEndDate)
    Pbcounts = np.zeros( SimEndDate)
    
    PaybacksNM = np.zeros( SimEndDate)
    PbcountsNM = np.zeros( SimEndDate)
    print 'Number of Home Owners:', len(HomeOwnerList)
    CurDate = 0
    ToU_base = np.array((SummerToU, WinterToU))
    FiT_base = np.array((FiTlowPV, FiTmidPV))
    CurrentScen = surveyScenarios.Scenario(FiT_base, ToU_base, 1, BattCost, Updater, 'payback calculation')
    while CurDate < SimEndDate:
          
        CurrentScen.UpdateVariables()       
        #Estimate payback fro different systems
          
        for ho in HomeOwnerList:
            for pvopt in PVs:
                for battopt in Batts:
                    purvars = CurrentScen.purchaseSavings(pvopt, ho.load, battopt)
                    if purvars[1]>=0:
                        Paybacks[CurDate]+=purvars[1]
                        Pbcounts[CurDate]+=1
                    purvarsNM = CurrentScen.paybackNM(pvopt, ho.load, battopt)
                    if purvarsNM[1]>=0:
                        PaybacksNM[CurDate]+=purvarsNM[1]
                        PbcountsNM[CurDate]+=1
        CurDate +=1
    print CurDate, sv['name'],':',Pbcounts
    AllScens.append({'scenario': 'FiT Payback','payback': Paybacks/Pbcounts})
    AllScensNM.append({'scenario': 'Net Metering Payback','payback': PaybacksNM/PbcountsNM})
with open('Ontario_Paybacks','wb') as f:
    pickle.dump({'FiT': AllScens, 'NM': AllScensNM}, f)
AllRes = pickle.load(open('Ontario_Paybacks','rb'))
for res in AllScens:
    plt.plot(res['payback'],label = res['scenario'])
for res in AllScensNM:
    plt.plot(res['payback'],label = res['scenario'])
plt.legend()
x1,x2,y1,y2 = plt.axis()
plt.xlabel('6-month Epoch', size = 15)
plt.ylabel('Payback (years)', size = 15)
# plt.axis((x1,x2,0,100))
plt.title('Average System Paybacks', size =20)
plt.show()