from __future__ import division
import csv
import numpy as np
import datetime as dt
import math
import pylab as pl
#import MySQLdb as mdb

#hourOfYr = 0 #this should be +=1 every hour
HomePVLimit = 10 #Limit on the PV capacity for a home
FiTChangeCap = 10 #Capacity at which FiT changes
#SolarIrrad
SummerToU = np.array([0.08,0.08,0.08,0.08,0.08,0.08,0.08,0.122, 0.122,0.122,\
                      0.122,0.161,0.161,0.161,0.161,0.161,0.161,0.122,0.122,0.08,0.08,0.08,0.08,0.08])

WinterToU = np.array([0.08,0.08,0.08,0.08,0.08,0.08,0.08,0.161, 0.161,0.161,\
                      0.161,0.122,0.122,0.122,0.122,0.122,0.122,0.161,0.161,0.08,0.08,0.08,0.08,0.08])

PVkWArea = 0.15 #Change this to represent actual kW
TodaysDate = dt.datetime(2015, 1,1,0) #year, month, day, hour
CurrentDate = 0
SimEndDate = 20 #Assuming epochs, each epoch could be 3months for example
Epoch = 6#Months
PVefficiency = 0.2
Summer = [5,6,7,8,9,10]
Weekend = [5,6]

#global CurrentDate 

#Wang, Di, et al. "Energy storage in datacenters: what, where, and how much?
BatteryChargeRate = 0.33 #kW/kWh of battery 
BatteryDischargeRate = 1.67 #kW/kWh of battery
BatteryChargeEff = 0.85
BatteryDischargeEff = 1
BatteryCycles = 5000
UnitBattery = 4#kWh, additional batteries for purchase with PV system
UnitPV = 3
DoD = 0.2 #Depth of discharge \cite check EV paper citation


disc = 0.05 #Discount rate
Life = 20 #Project lifetime based on FiT contract length
DNI = np.random.randint(10,size=8760)/10 #Just a Test DNI, kW/m2
battLife = 10 #years

FiTlowPV = 0.384 # $/kWh
FiTmidPV = 0.343
kWperm2 = 0.162 #


#Collect DNI from file
refPVSize = 3 #kW
PVGen_file = open("3kWOutput.csv", "rb")
PVGen_reader = csv.reader(PVGen_file)
PVkW = []
ConversionRate = 1 #Eur to cad, not applicable here
for gen in PVGen_reader:
    PVkW.append(float(gen[0]))
# pl.plot(PV3kW)
# pl.show()

#PV3kWLife = np.array(PVkW * SimEndDate)

PVGen_file.close()

# db = mdb.connect(host="typhoon.cs.uwaterloo.ca", user="a2adepet", passwd="Adeda12!", db="essex_annotated")
#  
# cur = db.cursor()


    
    
def WillBuy(PV, battery):
    
    return 1 #Should return buying decision and savings associated

def estimateSavings(PVCapacity, load, battery, battkWhCost, ToU_, FiT_):
    '''returns annual savings from recurring electricity and battery expenses
        without considering capital expenditure
    '''
    if PVCapacity <=10:
        FiT = FiT_[0]
    else:
        FiT = FiT_[1]
    battUsable = battery * (1-DoD)
    Start = dt.datetime(2015,1,1,0)
    sale = sum(PVkW) /3* PVCapacity * FiT
    totalConsumption = 0
    for hr in range(8760):
    #hourOfYr = int((TodaysDate - dt.datetime(TodaysDate.year,1,1,0)).seconds/3600)
    
        #Need to define DNI over 8760 hours
        if Start.month in Summer:
            ToU = ToU_[0] #Summer ToU
        else:
            ToU =  ToU_[1]
        
        consumption = 0
        if ToU[Start.hour] == max(ToU) and Start.weekday() not in Weekend: 
            #Use battery
            availableEnergy = battUsable * BatteryDischargeEff * BatteryDischargeRate 
            if availableEnergy >= load[hr]:
                
                battUsable -= load[hr] *BatteryDischargeEff
            else: #Change has to be made if BatteryDischargeRate <1
                consumption = ToU[Start.hour] * (load[hr] - battUsable)
                battUsable = 0
        else:
            #Charge battery
            hourCharge = BatteryChargeRate * battery
            oldbattUsable = battUsable
            battUsable = min(battery * (1-DoD), battUsable + hourCharge)
            if Start.weekday() not in Weekend:
                consumption = ToU[Start.hour] * (load[hr] + (battUsable - oldbattUsable))
            else:
                consumption = min(ToU) * (load[hr] + (battUsable - oldbattUsable))
        
        totalConsumption += consumption
               
        Start += dt.timedelta(minutes = 60)
    
    
    #additionalFees = 20*12 #Assuming monthly connection fees of 20$
    #savings -= additionalFees
    batteryAnnual = amortizeBatteryCosts(battery, battkWhCost)
    savingAfterBatt = sale - (totalConsumption + batteryAnnual)
    
    return savingAfterBatt


def estimateSavingsOffGrid(PVCapacity, load, battery, kWhCost, ToU_, FiT_, PVPriceMultiplier):
    '''In the case of disconnecting from the grid,
    how much is saved annually'''
    feasible = 1
    battUsable = battery * (1-DoD)
    for hr in range(8760):
        
        
        gen = PVkW[hr] /3* PVCapacity
        loadDiff = gen - load[hr]
        if loadDiff<0:
            #get from battery
            fromBatt = battUsable + loadDiff
            if fromBatt <0:
                feasible = 0
                break
            else:
                battUsable += loadDiff
        else:
            battUsable = min(loadDiff + battUsable, battery * (1-DoD))
    
    expensesOnGrid = -estimateSavings(0, load, 0, kWhCost, ToU_, FiT_) 
    PVprice = getPVCapex(PVCapacity) * PVPriceMultiplier
    expensesOffGrid = amortizeBatteryCosts(battery, kWhCost) + amortizePVCosts(PVprice)
    
    #print expensesOnGrid, expensesOffGrid
    saved = expensesOnGrid - expensesOffGrid
    return [feasible, saved]
    
  

def amortizeBatteryCosts(batterySize, kWhCost):
    '''
    Estimate annual cost from using battery of batterySize kWh 
    with kWHCost ($/kWh)
    
    Assuming single Charge/Discharge per day for both On and Off Grid
    On Grid: Charge during offpeak and discharge during peak
    Off Grid: Charge during day (sunshine) and discharge at night
    '''
#     newBattLife = int(BatteryCycles/365)
    replace = int(math.floor((Life-1)/battLife))
    P=0
    for r in range(replace):
        
        P+=  batterySize* kWhCost/(1+disc)**((r+1)*battLife)
    #Now spread cost over project lifetime
    amortized = P* (disc + disc/(-1 + (1+disc)**Life))
    return amortized


def amortizePVCosts(pvPrice):
    
#     newBattLife = int(BatteryCycles/365)
    pvLife = 20

    amortized = pvPrice/pvLife* (disc + disc/(-1 + (1+disc)**pvLife))
    return amortized


def surveyNumbers(PVkW, batterySize, load, PVCapex, batteryUnitCost,ToU_, FiT_):
    #Get Savings from using PV
    saved1 = estimateSavings(PVkW, load, batterySize,ToU_, FiT_) 
    saved2 = estimateSavings(0, load, 0,ToU_, FiT_)
    saved = saved1 - saved2
    
    batteryAnnual = amortizeBatteryCosts(batterySize, batteryUnitCost)
    #print saved, batteryAnnual
    annualSavings = saved - batteryAnnual
    Capex = PVCapex + batterySize* batteryUnitCost
    payback = int(Capex/annualSavings)
    
    return payback


def getPVCapex(PVCapacity):
    ''' http://www.solacity.com/pvinstall.htm
    '''
    if PVCapacity <= 2:
        Capex =  PVCapacity * 6230
    elif PVCapacity <= 3:
        Capex =  PVCapacity * 4870
    elif PVCapacity <= 4:
        Capex =  PVCapacity * 4240
    elif PVCapacity <= 5:
        Capex =  PVCapacity * 3850
    elif PVCapacity <= 7:
        Capex =  PVCapacity * 3500
    else:
        Capex =  PVCapacity * 3140
    
    return Capex

def GetMeterReadings(MeterID):
    #Collect readings for a particular MeterID and adjust for missing days
    
    cur.execute("select read_datetime, Reading from SmartMeterReadings where MeterID = "+str(MeterID)+" limit 8760")

    AllLoads = []
#     AllDays = []
    FinalLoads = []
    sampleLoads = [[],[],[],[],[],[],[]]
#     for d in range(365):
#         if d==0:
#             firstDay = cur.fetchall()[0][0].date()
#             AllDays.append(firstDay.date())
#         else:
#             firstDay+= dt.timedelta(minutes = 1440)
#             AllDays.append(firstDay)
    tempLoad=[]
    for row in cur.fetchall():
        tempLoad.append(row)
    #print tempLoad
    for day in range(int(len(tempLoad)/24)):
        todaysLoad = []
        for hr in range(24):
            
            todaysLoad.append(tempLoad[day*24 + hr][1])
        todaysDate = tempLoad[day*24][0].date()
#         if tempLoad[day*24][0].hour !=0:
#             print tempLoad[day*24][0]
        AllLoads.append([todaysDate,todaysLoad])
    
    
    #Now fix missing days
    if not tempLoad or len(tempLoad) <8760:
        return 0
    today = tempLoad[0][0].date()
    
    for d in range(365):
        #print AllLoads[0]
        if today==AllLoads[0][0]:
            FinalLoads += AllLoads[0][1]
            sampleLoads[today.weekday()] = AllLoads[0][1]
            AllLoads.pop(0)
        else:
            FinalLoads += sampleLoads[today.weekday()]
        
        today+= dt.timedelta(minutes = 1440)
    #Correct Load date to start on Jan 1
    
    firstLoadDay = tempLoad[0][0].date()
    fLDays = 365-(firstLoadDay - dt.date(firstLoadDay.year, 1, 1)).days#.seconds/(24*3600)
    AdjustedLoad = FinalLoads[fLDays*24:] + FinalLoads[0:fLDays*24]
    
#     for d in range(365):
#         plt.plot(AdjustedLoad[d*24:(d+1)*24-1])
#     plt.show()
    return AdjustedLoad