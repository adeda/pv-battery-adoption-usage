from __future__ import division
from griddefection import *
import surveyScenarios
import owners
import outputs
from importSurvey import Indep, Respondents
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import pickle
import random
import csv
import pvbattsys
import copy
import math

#Initialize Agents and Environmental Vars
NumberOfHomeOwners = len(Respondents)

ElectricityGeneration = np.zeros(8760 * SimEndDate)


#Changes: number of replicas, scenarios, threshold
def getPriceDecay(y,x):
	
	prices = np.linspace(y,y, SimEndDate+1)
	decay = []
	
	decayfactor = 10** (math.log10(x/y)/(10))
	for i in range(SimEndDate+1):
		decay.append(round(prices[i]* decayfactor**i,2))
	
	return np.asarray(decay)
	
	
SocialSizes = []
agentreplicas = 8
AllHomeOwnerIDs = np.linspace(0, (NumberOfHomeOwners-1)*agentreplicas, (NumberOfHomeOwners-1)*agentreplicas)
AllHomeOwnerIDs = AllHomeOwnerIDs.astype(int)
TVars_ = [0.42,0.14]

#Create price decays
pvprice_base = getPriceDecay(1,0.58)
# plt.plot(pvprice_base, label = 'Base Case PV Price', linestyle = '--', linewidth = 4)
# plt.show()
pvprice_2 = getPriceDecay(1,0.29)
battprice_base = getPriceDecay(1,0.5)
battprice_2 = getPriceDecay(1,0.25)
# print pvprice_base
# print pvprice_2
# plt.plot(battprice_base, label = 'Base Case Battery Price', linestyle = '-.', linewidth = 3)
# 
# plt.plot(battprice_2, label = 'Reduced Battery Price', linestyle = '-.', linewidth = 4)
# plt.plot(pvprice_2, label = 'Reduced PV Price', linestyle = '--', linewidth = 4)
# plt.plot(np.linspace(1,0,SimEndDate+1), label = 'Base Case FiT', linestyle = ':', linewidth = 4)
# plt.plot(np.linspace(1,0.3,SimEndDate+1), label = 'Slowly Reduced FiT', linestyle = ':', color = 'k', linewidth = 4)
# plt.plot(np.linspace(1,2.4,SimEndDate+1), label = 'ToU')
# plt.plot(np.linspace(1,6,SimEndDate+1), label = 'Increased ToU', linestyle = ':', linewidth = 2)
# plt.legend(loc = 1, fontsize = 30)
# plt.ylabel('Coefficient', size = 35)
# plt.title('PV Price Coefficients in Base Case and Alternative Scenarios', size = 40)
# plt.xlabel('Time (6-month epochs)', size = 35)
# XTick = ['Jan 2015', '', '', '', 'Jan 2017', '', '', '', 'Jan 2019', '', ''\
#          , '','Jan 2021', '', '', '', 'Jan 2023', '', '', '', 'Jan 2025', '', 'Jan 2026' ]
# plt.xticks(np.arange(0,21), XTick, size = 25)
# plt.yticks(size = 25)
# plt.show()
ScenVars_ = [{'name': 'base case', 'BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
			'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
			 			
 
 			
 			 {'name': 'reduced battery price','BattPrice': battprice_2, 'FiT': np.linspace(1,0,SimEndDate+1),\
 			'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
   			
 			{'name': 'less reduced fit','BattPrice': battprice_base, 'FiT': np.linspace(1,0.3,SimEndDate+1),\
			'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
   			
 			{'name': 'increased tou','BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
 			'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,6,SimEndDate+1)},
  			
  			{'name': 'increased tou and reduced pv price','BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
 			'PVPrice' :pvprice_2,   'ToU' : np.linspace(1,6,SimEndDate+1)},
  			 			
 			{'name': 'increased tou and reduced battery price','BattPrice': battprice_2, 'FiT': np.linspace(1,0,SimEndDate+1),\
 			'PVPrice' :pvprice_base,   'ToU' : np.linspace(1,6,SimEndDate+1)},
  			
			{'name': 'reduced pv price','BattPrice': battprice_base, 'FiT': np.linspace(1,0,SimEndDate+1),\
			'PVPrice' :pvprice_2,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
 	
 			 {'name': 'reduced pv and battery price','BattPrice': battprice_2, 'FiT': np.linspace(1,0,SimEndDate+1),\
 			'PVPrice' :pvprice_2,   'ToU' : np.linspace(1,2.4,SimEndDate+1)},
			 
			 ]

# ScenVars_ = [{'name': 'base case', 'BattPrice': np.linspace(1,0.5, SimEndDate+1), 'FiT': np.linspace(1,0.33,SimEndDate+1),\
# 			'PVPrice' :np.linspace(1,0.58,SimEndDate+1),   'ToU' : np.linspace(1,1.2,SimEndDate+1)},
# 			 			
#  
#  			
#  			 {'name': 'reduced battery price','BattPrice': np.linspace(1,0.2, SimEndDate+1), 'FiT': np.linspace(1,0.33,SimEndDate+1),\
#  			'PVPrice' :np.linspace(1,0.58,SimEndDate+1),   'ToU' : np.linspace(1,1.2,SimEndDate+1)},
#    			
#  			{'name': 'less reduced fit','BattPrice': np.linspace(1,0.5, SimEndDate+1), 'FiT': np.linspace(1,0.6,SimEndDate+1),\
# 			'PVPrice' :np.linspace(1,0.58,SimEndDate+1),   'ToU' : np.linspace(1,1.2,SimEndDate+1)},
#    			
#  			{'name': 'increased tou','BattPrice': np.linspace(1,0.5, SimEndDate+1), 'FiT': np.linspace(1,0.33,SimEndDate+1),\
#  			'PVPrice' :np.linspace(1,0.58,SimEndDate+1),   'ToU' : np.linspace(1,3,SimEndDate+1)},
#   			
#   			 			
#  			{'name': 'increased tou and reduced battery price','BattPrice': np.linspace(1,0.2, SimEndDate+1), 'FiT': np.linspace(1,0.33,SimEndDate+1),\
#  			'PVPrice' :np.linspace(1,0.58,SimEndDate+1),   'ToU' : np.linspace(1,3,SimEndDate+1)},
#   			
# 			{'name': 'reduced pv price','BattPrice': np.linspace(1,0.5, SimEndDate+1), 'FiT': np.linspace(1,0.33,SimEndDate+1),\
# 			'PVPrice' :np.linspace(1,0.3,SimEndDate+1),   'ToU' : np.linspace(1,1.2,SimEndDate+1)},
#  	
#  			 {'name': 'reduced pv and battery price','BattPrice': np.linspace(1,0.2, SimEndDate+1), 'FiT': np.linspace(1,0.33,SimEndDate+1),\
#  			'PVPrice' :np.linspace(1,0.3,SimEndDate+1),   'ToU' : np.linspace(1,1.2,SimEndDate+1)},
# 			 
# 			 ]

#def preSim(TVars):


# plt.hist(SocialSizes)
# plt.show()
#CurrentDate = 0

#Run simulation years
print 'Number of Home Owners:', NumberOfHomeOwners*agentreplicas
def runSim(HomeOwners, CurrentScenario):
	NetLoadOnGrid = np.array([])
	Generation = np.array([])
	while outputs.CurrentDate < SimEndDate:
	
		CurrentScenario.UpdateVariables()	   
		#Update social network adoption fraction 
# 		withPV = 0
		for ho in HomeOwners:
			ho.UpdateSocialPV(HomeOwners)
# 			if ho.PVBatt:
# 				withPV +=1
# 		populationPV = withPV/len(HomeOwners)
		#print 'Popn2' , populationPV, outputs.PVAdoptionNum[0]
			
		#Purchase PV Systems
		for ho in HomeOwners:
# 			if ho.T < populationPV:
			ho.buyPV(CurrentScenario)
			
		
		#Calculate Output Variables for each agent
		
		firstLoad = 1
		for ho in HomeOwners:
			#Calculate start and end hours
			netLoad, generation = ho.PVBattLoad(CurrentScenario)
			if firstLoad:
				epochNetLoad = netLoad
				epochGen = generation
				firstLoad = 0
			else:
				epochNetLoad += netLoad
				epochGen += generation
		
		NetLoadOnGrid = np.hstack((NetLoadOnGrid , epochNetLoad))
		Generation = np.hstack((Generation, epochGen))
		
# 		plt.plot(Generation)
# 		plt.show()
		
# 		plt.plot(NetLoadOnGrid)
# 		plt.show()		
			#Update Environmental Variables
		
		outputs.CurrentDate +=1
	return NetLoadOnGrid, Generation
	
#	 print 'PV Adoption (%)', outputs.PVAdoptionNum/FinalHO * 100
#	 plt.plot(np.cumsum(outputs.PVAdoptionNum)/FinalHO * 100)
#	 plt.xlabel('6-month epoch')
#	 plt.ylabel('PV Adopters (%)')
#	 plt.show()
	
SimRun = 0
#SimRes = [[]]* len(ScenVars_)
SimRes = []
simscencount = 41
while SimRun < 40:
	

	#Starting Values
	HomeOwners_main, PVAdNum, PVAdkW = outputs.init(TVars_, NumberOfHomeOwners, agentreplicas, 'main')
	FinalHO =  len(HomeOwners_main)
	svcount = 0
	#print PVAdNum
	for sv in ScenVars_:
		global CurrentScen_
		HomeOwners_ = copy.deepcopy(HomeOwners_main)
		#Starting Values
		outputs.PVAdoptionkWFiT = copy.deepcopy(PVAdNum) #PV Adoption in KW	
		outputs.PVAdoptionNumFiT = copy.deepcopy(PVAdkW) #PV Adoption in number of installed systems
		
		outputs.PVAdoptionkWNetM = copy.deepcopy(PVAdNum) #PV Adoption in KW	using the net metering scheme
		outputs.PVAdoptionNumNetM = copy.deepcopy(PVAdkW) #PV Adoption in number of installed systems using the net metering scheme
		
		outputs.PVAdoptionkW = copy.deepcopy(PVAdNum) #Total PV Adoption in KW
		outputs.PVAdoptionNum = copy.deepcopy(PVAdkW) #Total PV Adoption in number of installed systems
		
		outputs.BatteryAdoptionkWh = np.zeros(SimEndDate+1) #in kWh
		outputs.BatteryAdoptionNum = np.zeros(SimEndDate+1)
		#outputs.SystemPaybacks = copy.deepcopy(SysPaybacks)
		outputs.CurrentDate = 0
		
		#print 'at start', outputs.PVAdoptionNum
		
		ToU_base = np.array((SummerToU, WinterToU))
		FiT_base = np.array((FiTlowPV, FiTmidPV))

		Updater= {'FiT': sv['FiT'].tolist(), 'ToU': sv['ToU'].tolist(), 'Battery': sv['BattPrice'].tolist(), 'PV': sv['PVPrice'].tolist()}
		
		BattCost = 1500
		CurrentScen_ = surveyScenarios.Scenario(FiT_base, ToU_base, 1, BattCost, Updater, sv['name'])
		
		NetLoadOnGrid_, Generation_  = runSim(HomeOwners_, CurrentScen_)
		for ho in HomeOwners_:
			if ho.PVBatt:
				outputs.PVAdoptionNum[ho.PVBatt.date] +=1
				outputs.PVAdoptionkW[ho.PVBatt.date] += ho.PVBatt.PV
				if ho.PVBatt.battery >0:
					outputs.BatteryAdoptionkWh[ho.PVBatt.date] += ho.PVBatt.battery
					outputs.BatteryAdoptionNum[ho.PVBatt.date] += 1
				
				if ho.PVBatt.contract == 'FiT':
					outputs.PVAdoptionNumFiT[ho.PVBatt.date] +=1
					outputs.PVAdoptionkWFiT[ho.PVBatt.date] += ho.PVBatt.PV
				
				else:
					outputs.PVAdoptionNumNetM[ho.PVBatt.date] +=1
					outputs.PVAdoptionkWNetM[ho.PVBatt.date] += ho.PVBatt.PV
					
		
# 		plt.plot(NetLoadOnGrid_)
# 		plt.show()
# 		SimRes.append({'name': sv['name'],'simrun':SimRun, 'PVNum': outputs.PVAdoptionNum, 'PVkW': outputs.PVAdoptionkW, \
# 					'PVNumFiT': outputs.PVAdoptionNumFiT, 'PVkWFiT': outputs.PVAdoptionkWFiT, 'PVNumNetM': outputs.PVAdoptionNumNetM, 'PVkWNetM': outputs.PVAdoptionkWNetM,\
# 					   'BattNum':outputs.BatteryAdoptionNum,'BattkWh': outputs.BatteryAdoptionkWh, 'Load': NetLoadOnGrid_, 'Generation': Generation_ ,\
# 					   'Paybacks': outputs.SystemPaybacks/outputs.PurchaseAtt})
		
		curRes= {'name': sv['name'],'simrun':SimRun, 'PVNum': outputs.PVAdoptionNum, 'PVkW': outputs.PVAdoptionkW, \
					'PVNumFiT': outputs.PVAdoptionNumFiT, 'PVkWFiT': outputs.PVAdoptionkWFiT, 'PVNumNetM': outputs.PVAdoptionNumNetM, 'PVkWNetM': outputs.PVAdoptionkWNetM,\
					   'BattNum':outputs.BatteryAdoptionNum,'BattkWh': outputs.BatteryAdoptionkWh, 'Load': NetLoadOnGrid_, 'Generation': Generation_ ,\
					   'Paybacks': outputs.SystemPaybacks/outputs.PurchaseAtt}
		
		svcount+=1
		with open('OntarioPV_subsidy'+str(simscencount),'wb') as f:
			pickle.dump(curRes, f)
		simscencount+=1
	SimRun+=1
# with open('OntarioPV_final','wb') as f:
# 	pickle.dump(SimRes, f)
# for scenRes in SimRes:
# 	for srun in scenRes:
# 		print 'Num Home Owners 2: ', len(HomeOwners_)
# 		print 'PV Adoption (%)', srun['PVNum']
# 		plt.plot(np.cumsum(srun['PVNum'])/FinalHO * 100)
# 
# plt.show()
# for aload in outputs.ActualLoads:
#	 ActualLoadOnGrid += np.array(aload)
	
# plt.plot(outputs.NetLoadOnGrid)
# plt.show()
#runSim()
# FinalHO =  len(HomeOwners_)
# print 'PV Adoption (%)', outputs.PVAdoptionNum/FinalHO * 100
# plt.plot(np.cumsum(outputs.PVAdoptionNum)/FinalHO * 100)
# plt.xlabel('6-month epoch')
# plt.ylabel('PV Adopters (%)')
# #plt.plot(np.cumsum(outputs.PVAdoptionNum))
# plt.show()
# plt.plot(np.cumsum(outputs.BatteryAdoptionNum)/FinalHO * 100)
# plt.xlabel('6-month epoch')
# plt.ylabel('Battery Adopters (%)')
# plt.show()
