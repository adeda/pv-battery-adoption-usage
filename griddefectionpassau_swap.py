from __future__ import division
import csv
import numpy as np
import datetime as dt
import math
import pylab as pl


HomePVLimit = 10 #Limit on the PV capacity for a home
kWprice = 0.26
FitRate = 0.1288

TodaysDate = dt.datetime(2015,1,1,0) #year, month, day, hour
SimEndDate = 20 #Assuming epochs, each epoch could be 3months for example
Epoch = 6 #Months

ConversionRate = 1.4 #Eur to cad
#Wang, Di, et al. "Energy storage in datacenters: what, where, and how much?
BatteryChargeRate = 0.33 #kW/kWh of battery 
BatteryDischargeRate = 1.67 #kW/kWh of battery
BatteryChargeEff = 0.85
BatteryDischargeEff = 1
BatteryCycles = 5000
UnitBattery = 4#kWh, additional batteries for purchase with PV system
UnitPV = 3
DoD = 0.2 #Depth of discharge \cite check EV paper citation

disc = 0.05 #Discount rate
Life = 20 #Project lifetime based on FiT contract length
#DNI = np.random.randint(10,size=8760)/10 #Just a Test DNI, kW/m2
battLife = 10 #years


FiTlowPV = 0.1288
FiTmidPV = 0.1222

kWperm2 = 0.162
FixedFee= 10 #fixed monthly fee
#DNI = np.random.randint(10,size=8760)/10 #Just a Test DNI, kW/m2, 24*365=8760
#price = 0.20 # kWh price from grid
refPVSize = 3 #kW
PVGen_file = open("3kWOutput.csv", "rb")
PVGen_reader = csv.reader(PVGen_file)
PVkW = []
for gen in PVGen_reader:
    x=gen[0]
    #f = float(x[16:])
    PVkW.append(float(x))
    #print (x[16:])
#pl.plot(PV3kW)
#pl.show()
PVGen_file.close()



def WillBuy(PV, battery):
    
    return 1 #Should return buying decision and savings associated
'''
def estimateSavings(PVCapacity, load, battery, battkWhCost, kWprice, FiT_):
    PV Area is the m2 size of the panel
    load is a 8760 array of kWh load values
    battery = size of battery in kWh. Assumed to be full at the start 


    if PVCapacity <=10:
        FiT = FiT_[0]
    else:
        FiT = FiT_[1]

    battUsable = battery * (1-DoD)
    Start = dt.datetime(2015,1,1,0)
    sale = sum(PV3kW)/10* PVCapacity * FiT
    savings = 0
    consumption = 0
    for hr in range(8760):
    #hourOfYr = int((TodaysDate - dt.datetime(TodaysDate.year,1,1,0)).seconds/3600)
    
        # need to consider own consumption because Fit is different
        # sale = (PVefficiency * DNI[hr] * PVArea - load[hr])* max[FitRate]+ (load[hr]* min[FitRate]) #Need to define DNI over 8760 hours
        RenEnergy = PV3kW[hr] /10* PVCapacity
        #consumption = 0
        if RenEnergy <= load[hr]:
            RestLoad=load[hr]-RenEnergy
            availableEnergy = battUsable * BatteryDischargeEff * BatteryDischargeRate
            if availableEnergy>= RestLoad:
                battUsable -= RestLoad*BatteryDischargeEff
            else:
                consumption += kWprice * (RestLoad - battUsable)
                battUsable = 0
        else:
            RestRen = RenEnergy - load[hr]
            #hourCharge = BatteryChargeRate * battery                       
            battUsable = min(battery, battUsable + RestRen)                  
        Start += dt.timedelta(minutes = 60)
		
    batteryAnnual = amortizeBatteryCosts(battery, battkWhCost)
    #print sale,consumption
    savingAfterBatt = sale - (consumption + batteryAnnual)
    
    return savingAfterBatt
'''

def estimateSavings(PVCapacity, load, battery, battkWhCost, kWprice, FiT_):
    '''PV Area is the m2 size of the panel
    load is a 8760 array of kWh load values
    battery = size of battery in kWh. Assumed to be full at the start

    '''
    if PVCapacity <=10:
        FiT = FiT_[0]
    else:
        FiT = FiT_[1]

    battUsable = battery * (1-DoD)
    Start = dt.datetime(2015,1,1,0)
    #sale = sum(PV3kW)/10* PVCapacity * FiT
    savings = 0
    consumption = FixedFee*12
    sale =0
    #load=load*[0.004]
    for hr in range(8760):
    #hourOfYr = int((TodaysDate - dt.datetime(TodaysDate.year,1,1,0)).seconds/3600)

        # need to consider own consumption because Fit is different
        # sale = (PVefficiency * DNI[hr] * PVArea - load[hr])* max[FitRate]+ (load[hr]* min[FitRate]) #Need to define DNI over 8760 hours
        RenEnergy = PVkW[hr] /refPVSize* PVCapacity
        #sale =0
        #consumption = 0
        #load[hr]=load[hr]*0.004

        #print "hour", hr
        #print "RenEnergy", RenEnergy

        if RenEnergy <= load[hr]:
            RestLoad=load[hr]-RenEnergy
            availableEnergy = battUsable * BatteryDischargeEff * BatteryDischargeRate
            if availableEnergy>= RestLoad:
                battUsable -= RestLoad*BatteryDischargeEff
                #print "hour", hr
                #print battUsable
            else:
                consumption += kWprice * (RestLoad - battUsable*BatteryDischargeEff)
                battUsable = 0
                #print "hour", hr
                #print battUsable
                #print "RenEnergy", RenEnergy

        else:
            RestRen = RenEnergy - load[hr]
            #print "RestRen"
            #print RestRen
            oldbattUsable = battUsable
            hourCharge = BatteryChargeRate * battery
            battUsable = min(battery*(1-DoD),battUsable+hourCharge ,battUsable + RestRen)
            #print "hour", hr
            #print battUsable
            Differ=battUsable-oldbattUsable
            if Differ < RestRen:
                sale += (RestRen-Differ) * FiT
        Start += dt.timedelta(minutes = 60)

    batteryAnnual = amortizeBatteryCosts(battery, battkWhCost)
   # print kWprice
   # print sale,'     ',consumption, '    ',batteryAnnual
   # print 'batteryAnnual',batteryAnnual
    savingAfterBatt = sale - (consumption + batteryAnnual)
   # print savingAfterBatt
    return savingAfterBatt

def estimateSavingsOffGrid(PVCapacity, load, battery, kWhCost, kWprice, FiT_):
    '''In the case of disconnecting from the grid,
    how much is saved annually'''
    #load=load*[0.004]
    feasible = 1
    battUsable = battery * (1-DoD)
    for hr in range(8760):                
        gen = PV3kW[hr] /10* PVCapacity
        #load[hr]=load[hr]*0.004
        loadDiff = gen - load[hr]
        if loadDiff<0:
            #get from battery
            availableEnergy = battUsable * BatteryDischargeEff * BatteryDischargeRate
            fromBatt = availableEnergy + loadDiff
            if fromBatt <0:
                feasible = 0
                break
            else:
                battUsable += loadDiff*BatteryDischargeEff
                #battUsable += loadDiff
        else:
            battUsable = min(loadDiff + battUsable, battery * (1-DoD))
    
    #print 'off grid'
    expensesOnGrid = -estimateSavings(PVCapacity, load, battery, kWhCost, kWprice, FiT_)
    expensesOffGrid = amortizeBatteryCosts(battery, kWhCost)
    #print expensesOnGrid, expensesOffGrid
    saved = expensesOnGrid - expensesOffGrid
    #print expensesOnGrid, expensesOffGrid, saved
    return [feasible, saved]	
	
	
def surveyNumbers(PVkW, batterySize, load, PVCapex, batteryUnitCost, kWprice, FiT_):
    #Get Savings from using PV
    saved1 = estimateSavings(PVkW, load, batterySize,kWprice, FiT_) 
    saved2 = estimateSavings(0, load, 0,kWprice, FiT_)
    #print 'saved1: '+ repr(saved1)
    #print 'saved2: '+ repr(saved2)
    saved = saved1 - saved2
    #print 'saved: '+ repr(saved)
    batteryAnnual = amortizeBatteryCosts(batterySize, batteryUnitCost)
    #print 'batteryAnnual:' + repr(batteryAnnual)
    annualSavings = saved - batteryAnnual
    Capex = PVCapex + batterySize* batteryUnitCost

    payback = int(Capex/annualSavings)

    return payback
def getPVCapex(PVCapacity):
    '''Source: Roland Berger, Diverse Quellen, statista 2014, Mr.Feilmeier

    if PVCapacity <= 2:
        Capex =  PVCapacity * 2550
    elif PVCapacity <= 3:
        Capex =  PVCapacity * 1988
    elif PVCapacity <= 4:
        Capex =  PVCapacity * 1730
    elif PVCapacity <= 5:
        Capex =  PVCapacity * 1600
    elif PVCapacity <= 7:
        Capex =  PVCapacity * 1430
    else:
        Capex =  PVCapacity * 1100

    return Capex
    '''
    if PVCapacity <= 2:
        Capex =  PVCapacity * 1900
    elif PVCapacity <= 3:
        Capex =  PVCapacity * 1800
    elif PVCapacity <= 4:
        Capex =  PVCapacity * 1700
    elif PVCapacity <= 5:
        Capex =  PVCapacity * 1600
    elif PVCapacity <= 7:
        Capex =  PVCapacity * 1450
    else:
        Capex =  PVCapacity * 1300

    return Capex

def amortizeBatteryCosts(batterySize, kWhCost):
    '''
    Estimate annual cost from using battery of batterySize kWh
    with kWHCost ($/kWh)
    '''
    replace = int(math.floor((Life-1)/battLife))
    #print replace
    P=0
    for r in range(replace):
        #print r
        #f= (1+disc)**((r+1)*battLife)
        P+=  batterySize* kWhCost/(1+disc)**((r+1)*battLife)
        #print f
    #Now spread cost over project lifetime
    amortized = P* (disc + disc/(-1 + (1+disc)**Life))
    return amortized