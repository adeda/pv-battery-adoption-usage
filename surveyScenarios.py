from __future__ import division
from griddefectionpassau_swap import *

#from fromDatabase import *
#import pickle

#What if PV prices were halved and batteries/3? 
#Would it make sense to disconnect for the feasible configurations

class Scenario:
    
    def __init__(self, FiT, ToU, PVCapexMultiplier, BatteryPrice, Updater, name):
        
        self.FiT = FiT
        self.ToU = ToU
        self.PVCapexMultiplier = PVCapexMultiplier
        self.BPrice = BatteryPrice
        self.Updater = Updater #Variables for updating (multiplying) the scenario variables periodically
        self.BaseValues = {'FiT': FiT, 'ToU': ToU, 'PV': PVCapexMultiplier, 'Battery': BatteryPrice}
        self.name = name
        #self.PVEfficiency = PVEfficiency
        
        
    def fullDisconnectionSavings(self, loadData, PVCapacity, battery):
        
        saved = estimateSavingsOffGrid(PVCapacity, loadData, battery, self.BPrice, self.ToU, self.FiT, self.PVCapexMultiplier)
        return saved
    
    def getPVPrice(self, PVCapacity):
        
        PVPrice = getPVCapex(PVCapacity)*self.PVCapexMultiplier
        return PVPrice
    
    def purchaseSavings(self, PVCapacity, loadData, battery):
        
        savingWithPVBatt = estimateSavings(PVCapacity, loadData, battery, self.BPrice, self.ToU, self.FiT)
        savingWithoutPVBatt = estimateSavings(0, loadData, 0, self.BPrice, self.ToU, self.FiT)
        #print savingWithPV, savingWithoutPV
        saved =  savingWithPVBatt - savingWithoutPVBatt
        PVBattPrice = self.getPVPrice(PVCapacity) + battery* self.BPrice
        #payback = self.discountedPayback(PVBattPrice,saved) #Discounted Payback
        payback =PVBattPrice/saved #Simple payback
#         if payback < 0:
#             payback = 100
        gains = Life  * saved
        AnnualROI =  max(0, (gains - PVBattPrice)/PVBattPrice *100/Life) 
        return [PVBattPrice, payback, AnnualROI]
    
    def paybackNM(self, PVCapacity, loadData, battery):
        
        #Estimate normal bill without net metering
        normalBill = self.billFromLoad(loadData)
        
        #Estimate bill with net metering
        #PVGen = np.asarray(PV3kW)/refPVSize * PVCapacity
        PVGen = np.asarray(PVkW)/refPVSize * PVCapacity
        diffLoad = np.asarray(loadData) - PVGen
        diffBill = self.billFromLoad(diffLoad)
        
        #Calc payback
        sysCost = self.getPVPrice(PVCapacity) + battery* self.BPrice
        saved = normalBill - max(0, diffBill)
        payback = sysCost/saved
        
        return [sysCost, payback, 0.01] #0.01 Arbitrary ROI greater than zero, just to pass check in system selection by agent. However ROI not used in decision
        
    def billFromLoad(self, loadData):
        '''Estimate the electiricy bill in $ based on the ToU and load data'''
        Start = dt.datetime(2015,1,1,0)
        totalBill = 0
        prevMonth = 0
        for hr in range(8760):
            #hourOfYr = int((TodaysDate - dt.datetime(TodaysDate.year,1,1,0)).seconds/3600)
      
            #Need to define DNI over 8760 hours
            if prevMonth != Start.month:
                #Check if ToU scheme should be changed
                prevMonth = Start.month
                if Start.month in Summer:
                    ToU = self.ToU[0] #Summer ToU
                else:
                    ToU =  self.ToU[1]
                  
            totalBill += ToU[Start.hour] * loadData[hr]
  
            Start += dt.timedelta(minutes = 60)
        #totalBill = int(np.sum(loadData)*self.ToU)
         
        return totalBill
        
    def discountedPayback(self, Capex, annualProfit):
        discCash = 0
        paybackPeriod = 0
        for n in range(Life+1):
            if n !=0:
                discCash = annualProfit/(1+disc)**n #Cummulative Cash flow
            Capex -= discCash
            if Capex <=0:
                paybackPeriod = n-1 + (Capex + discCash)/discCash
                break
        return int(paybackPeriod)
    
    def UpdateVariables(self):
        
        self.FiT = self.BaseValues['FiT'] *  (self.Updater['FiT']).pop(1)
        self.ToU =  self.BaseValues['ToU'] *  (self.Updater['ToU']).pop(1)
        self.BPrice = self.BaseValues['Battery'] *  (self.Updater['Battery']).pop(1)
        self.PVCapexMultiplier = self.BaseValues['PV'] *  (self.Updater['PV']).pop(1)
        #print self.BPrice, self.PVCapexMultiplier, self.FiT, self.ToU 
        
        #print self.FiT, self.ToU, self.BPrice, self.PVCapexMultiplier
         
                