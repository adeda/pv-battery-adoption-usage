from __future__ import division
import surveyScenarios
from griddefection import *#FiTlowPV, FiTmidPV, SummerToU, WinterToU
import pickle
import numpy as np
import csv
import pylab as pl


#Collect load data from file
with open('AllLoads', 'rb') as f:
    AllLoads = pickle.load(f)
print len(AllLoads)
with open('feasibleConfigs', 'rb') as f:
    feasibleConfigs = pickle.load(f)

db = mdb.connect()
 
cur = db.cursor()

#Collect 10 sample business MeterIDs
# cur.execute("select MeterID from meter_business_type")
# BusinessMeters = []
# for row in cur.fetchall():
#     BusinessMeters.append(row[0])
# BusMeterID= np.array(BusinessMeters)

#Collect 10 sample residential MeterIDs
cur.execute("select MeterID from meter_res_structure_type limit 100")
ResidentialMeters = []
for row in cur.fetchall():
    ResidentialMeters.append(row[0])
ResMeterID= np.array(ResidentialMeters)
                     
  
ToUTimes= np.linspace(1,4,7)
FiTTimes = np.linspace(1,10,10)
PVPriceTimes = np.linspace(1/2,1,6)
BattPriceTimes = np.linspace(1/5,1,10)

fit=1
pvp=1
bpt =1
tou =1

Paybacks= []
DSaved = []
Base = []
#for fit in FiTTimes:
#for bpt in BattPriceTimes:
# for tou in ToUTimes:
#     ToU = [SummerToU*tou, WinterToU*tou]
#     FiT = [FiTlowPV /fit, FiTmidPV/fit]
#     
#      
#     currentScen = surveyScenarios.Scenario(FiT, ToU, pvp, 500/bpt) #FiT, ToU, PVCapexDivider, BatteryPrice
#     #load = [0]*8760
#     payb = 0
#     discs = 0
#     for h in range(len(AllLoads)):
#         purchaseSavings = currentScen.purchaseSavings(feasibleConfigs[h][1], AllLoads[h], feasibleConfigs[h][2])
#         #purchaseSavings = currentScen.purchaseSavings(feasibleConfigs[h][1], load, feasibleConfigs[h][2])
#         payb +=purchaseSavings[1]
#         #print sum(PV3kW) *0.384*feasibleConfigs[h][1]/3
#         fullDisconnectionSavings = currentScen.fullDisconnectionSavings(AllLoads[h], feasibleConfigs[h][1], feasibleConfigs[h][2])
#         discs+=fullDisconnectionSavings[1]
#     payb/=len(AllLoads)    
#     discs/=len(AllLoads)
#     Paybacks.append(payb)
#     DSaved.append(discs)
#     #Base.append(500/bpt)
#     Base.append(tou)
# 
# #print Paybacks
# pl.plot(Base,Paybacks)
# pl.show()
# 
# pl.plot(Base,DSaved)
# pl.show()
SampleSystems = [[2, 2], [4,3], [6,5], [10,7]] 
AvePaybacks = []
SystemCost = []
for system in SampleSystems:
    #for pvp in PVPriceTimes:
    ToU = [SummerToU, WinterToU]
    FiT = [FiTlowPV*1, FiTmidPV*1]
    
     
    currentScen = surveyScenarios.Scenario(FiT, ToU, 1, 760,[]) #FiT, ToU, PVCapexDivider, BatteryPrice
    #load = [0]*8760
    payb = 0
    discs = 0
    #for h in range(len(AllLoads)):
    for meter in ResMeterID:
        load = GetMeterReadings(meter)
        purchaseSavings = currentScen.purchaseSavings(system[0], load, system[1])
        #purchaseSavings = currentScen.purchaseSavings(system[0], AllLoads[h], system[1])
        payb +=purchaseSavings[1]
        
    SystemCost.append(purchaseSavings[0])   
    payb/=len(ResMeterID)
    #payb/=len(AllLoads)
    AvePaybacks.append(payb)
    #Base.append(500/bpt)
    #Base.append(tou)

pl.scatter(AvePaybacks, SystemCost)
pl.xlim(0,25)
pl.ylim(ymin=0)
pl.title('PV & Battery Systems')
pl.ylabel('System Cost ($)')
pl.xlabel('Payback Period (years)')
pl.show()
