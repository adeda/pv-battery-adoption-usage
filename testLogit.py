from __future__ import division
from griddefection import *
import statsmodels.api as sm
import numpy as np
import math
from scipy.stats import truncnorm
import pylab as pl
import random
import matplotlib.pyplot as plt
# 
prices = np.linspace(2,2, SimEndDate+1)
decay = []
decayfactor = 0.875
for i in range(SimEndDate+1):
    decay.append(prices[i]* decayfactor**i)
print decay
plt.plot(decay)
plt.show()
# 
# 
# spector_data = sm.datasets.spector.load()
# spector_data.exog = sm.add_constant(spector_data.exog, prepend=False)
#  
# print(spector_data.exog[20:25,:])
# #print(spector_data.endog[:5])
#  
# logit_mod = sm.Logit(spector_data.endog, spector_data.exog)
# logit_res = logit_mod.fit(disp=0)
# print('Parameters: ', logit_res.params)
#   
# margeff = logit_res.get_margeff()
# print(margeff.summary())
# print(logit_res.summary())

# def discountedPayback(Capex, annualProfit):
#     discCash = 0
#     paybackPeriod = 0
#     for n in range(Life+1):
#         if n !=0:
#             discCash = annualProfit/(1+disc)**n #Cummulative Cash flow
#         Capex -= discCash
#         if Capex <=0:
#             #print n
#             paybackPeriod = n-1 + (Capex + discCash)/discCash
#             break
#     return int(paybackPeriod)
# 
# for savings in [1000,1200,1400,1600,1800,2000,2500]:
#     print discountedPayback(10000,savings), 10000/savings
# Ts = []
# for a in range(1,1000):
#     a =  random.normalvariate(0.3,0.2)
#     Ts.append(a)
# pl.hist(Ts)
# pl.show()


# A=[ 13.0033557,  4.69798658,   3.52348993,  4.11073826,   4.78187919, 13.59060403,  21.97986577,  21.22483221,   4.36241611,   0.33557047,   0.        ]
#   
# B = [13.0033557  ,  2.85234899 ,  1.51006711  , 0.92281879,   1.4261745,  5.2852349  ,  8.47315436 , 13.0033557 ,  22.31543624 , 20.30201342 ,  0.        ]
# 
# C=[13.0033557 ,  2.68456376 ,  1.67785235 ,  1.67785235 ,  2.09731544,   6.96308725 , 11.32550336 , 19.2114094 ,  24.32885906 ,  7.96979866 ,  0.        ]
# 
# plt.plot(np.cumsum(A), label= '40% PV Price after 5years')
# plt.plot(np.cumsum(B), label= '60% PV Price after 5years')
# plt.plot(np.cumsum(C), label= '80% PV Price after 5years')
# plt.xlabel('6-month periods')
# plt.ylabel('PV-Adoption (%)')
# plt.show()
# 
# PVhistory = np.asarray([0,7,34,77,100,122,134,148,162,170,176])/176
# plt.plot(PVhistory)
# plt.show()
# 
# Trange = np.arange(1,7)
# SD = [0.1,0.2,0.3]



# for t in Trange:
#     for sd in SD:
#         social = []
#         for i in range(1000):
#             social.append(max(0,min(random.normalvariate(t/10,sd),1)))
#         plt.hist(social)
#         plt.title(str(t/10)+ ',' + str(sd))
#         plt.show()
social=[]
for i in range(10000):
    social.append(int(math.ceil(random.betavariate(1,3) *10)))
plt.hist(social)

plt.show()