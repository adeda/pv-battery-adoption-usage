from __future__ import division
import csv
import numpy as np
#import statsmodels.api as sm
import pandas as pd
from statsmodels.formula.api import logit
from deflection import newDeflection

from sklearn.linear_model import Ridge
from sklearn.metrics import r2_score

import matplotlib.pyplot as plt
import pylab as pl
#from mpl_toolkits.mplot3d import Axes3D
#import random
from matplotlib.mlab import PCA
import math

from sklearn.linear_model import RandomizedLogisticRegression, LinearRegression, lars_path, LassoLarsCV, LassoLars, LogisticRegression
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.preprocessing import normalize
import copy

#----------------------------#
#Code from https://gist.github.com/perrygeo/4512375
def scale_linear_bycolumn(rawpoints, high=100.0, low=0.0):
    mins = np.min(rawpoints, axis=0)
    maxs = np.max(rawpoints, axis=0)
    rng = maxs - mins
    return high - (((high - low) * (maxs - rawpoints)) / rng)

#End
#------------------------------#


#List of PV-Battery Options
# Payback, System COst, Annual ROI, Battery (1 without, 2with)

PVBattSystems = [(15, 12.460, 1.8, 1), #A
                 (10, 16.960, 5.1, 1), #B
                 (8, 21.000, 7.2, 1), #C
                 (18, 27.800, 0.7, 2), #C with battery
                 (7, 31.400, 8.6, 1), #D
                 (17, 43.300, 0.8, 2), #D with battery
                 
                 (11,   9.345,    4.1, 1),#A
                 (14,    10.345,    2.1, 2),#A with battery
                 (7,    12.720,    8.4, 1),#B
                 (9,    14.220,    5.7, 2),#B with battery
                 (6,    15.750,    11.2, 1),#C
                 (8,    17.750,    8.0, 2),#C with battery
                 (6,    23.550,    13.1, 1),#D
                 (7,    27.050,    9.0, 2),#D with battery
                 
                 (15, 9.345,    1.8, 1),#A
                 (10, 12.720,    5.1, 2),#A with battery
                 (8, 15.750,    7.2, 1),#B
                 (7, 23.550 ,   8.6, 2),#B with battery
                 (16, 9.745 ,   1.2, 1),#C
                 (11, 13.320,    4.2, 2),#C with battery
                 (9, 16.550,    6.1, 1),#D
                 ( 8, 24.950,    7.2, 2),#D with battery

                                  ]

crowd_file = open("survey/crowdflow_.csv", "rb")
crowd_reader = csv.reader(crowd_file)

ip_adds = []
Ontario = 0
for crowd in crowd_reader:
    if crowd[9]!= 'ON':
        ip_adds.append(crowd[11])
        Ontario +=1 
crowd_file.close()

survey_file = open("survey/solar_batt_.csv", "rb")
survey_reader = csv.reader(survey_file)
surveyresp= []
validResponses = 0

Respondents = []
Indep = []
PurchaseChoices = []

EPAList = [[],[],[],[],[]]
SocialInfluence = []
for res in survey_reader:
    #Test Question 
    EPA= False
    #print res[36]
    if res[38] == '3' and res[4] not in ip_adds:
        purcFrac = 0
        #How much do you know about solar panels
        #print res[10]
        
        #Do you currently have solar panels installed at your home/business?
        #print res[11]
        
        #Buying systems
        
        #print res[12:33]

        #Sentiments
        #print res[39]
        
        #Remove bad responses, i.e., click-through responses
        setBuy = res[12]
        goodResponse =  False
        for k in range(1,len(PVBattSystems)):
            if res[12] != res[12+k]:
                goodResponse = True
                break
        #Only Yes responses
        yesOnlyResp = 0
        for k in range(0,len(PVBattSystems)):
            if res[12+k] == '' or res[12+k] == '1':
                yesOnlyResp = 1
            else:
                yesOnlyResp = 0
                break
            
        if yesOnlyResp:
            for k in range(0,len(PVBattSystems)):
                if res[12+k] == '':
                    res[12+k] = '2'
        
        setEPA = res[40]
        goodEPA = False
        for i in range(40,55):
            if res[i]:
                EPA =  True
                if setEPA != res[i]:
                    goodEPA = True
            else:
                EPA = False
                break
            
        goodE =  False
        goodP =  False
        goodA =  False
        
        if EPA:
            setE = res[40]
            for k in range(40,45):
                if res[k] != setE:
                    goodE= True
                    break
            
            setP = res[45]
            for k in range(45,50):
                if res[k] != setP:
                    goodP= True
                    break
                    
            setA = res[50]
            for k in range(50,55):
                if res[k] != setA:
                    goodA= True
                    break
                    
        #print res[40:55]
        if goodResponse:# and EPA and goodEPA and goodE and goodP and goodA:
#             HomeOwnerEPA = ([int(res[40]), int(res[45]), int(res[50])])
#             BusOwnerEPA =([int(res[41]), int(res[46]), int(res[51])])
#             BuyingEPA = ([int(res[42]), int(res[47]), int(res[52])])
#             SolarPanelEPA= ([int(res[43]), int(res[48]), int(res[53])])
#             BatteryEPA= ([int(res[44]), int(res[49]), int(res[54])])
#               
#             EPAList[0].append([int(res[40]), int(res[45]), int(res[50])])
#             EPAList[1].append([int(res[41]), int(res[46]), int(res[51])])
#             EPAList[2].append([int(res[42]), int(res[47]), int(res[52])])
#             EPAList[3].append([int(res[43]), int(res[48]), int(res[53])])
#             EPAList[4].append([int(res[44]), int(res[49]), int(res[54])])
              
 
            D1 = 1#newDeflection(HomeOwnerEPA, BuyingEPA, SolarPanelEPA)
            D2 = 1#newDeflection(HomeOwnerEPA, BuyingEPA, BatteryEPA)

            #print D
            if res[10] and res[39] and res[37] and res[36] and res[35] and res[11]:
                validResponses+=1
                SocialInfluence.append(int(res[35]))
                
                Respondents.append((D1, D2, int(res[39]), int(res[37]), int(res[36]), int(res[10]), int(res[11]), int(res[35])))#, purcFrac #   , int(res[36])))#, 1))

                for j in range(0,len(PVBattSystems)):
                    
                    if res[12+j]:
                        if int(res[12+j]) == 1:
                            purcFrac +=1
#                             PurchaseChoices.append(0) #1 = Yes, 2= No
#                         else:
#                             PurchaseChoices.append(1)
                        
                        Indep.append((int(res[12+j])-1,) +  PVBattSystems[j] \
                                     + (D1, D2, int(res[39]), int(res[37]), int(res[36]), int(res[10]), int(res[11]), int(res[35]), int(res[10])* math.sqrt(D1), math.sqrt(D2)* PVBattSystems[j][3]))#, purcFrac #   , int(res[36])))#, 1))

# homeowners = np.mean(np.asarray(EPAList[0]), axis=0)
# busowners = np.mean(np.asarray(EPAList[1]), axis=0)
# buying = np.mean(np.asarray(EPAList[2]), axis=0)
# solarpanels = np.mean(np.asarray(EPAList[3]), axis=0)
# batts = np.mean(np.asarray(EPAList[4]), axis=0)
#     if validResponses >5:
#         break
# for i in range(0,len(Indep)):
#     homeepa = getEPA(homeowners)
#     solarpanelepa = getEPA(solarpanels)
#     buyingepa = getEPA(buying)
#       
#     D1 = newDeflection(homeepa, buyingepa, solarpanelepa)
#      
#      
#     Indep[i] +=  (D1,)


        
#print 'Valid Responses= ', validResponses
#print len(PurchaseChoices), len(Indep), len(EPAList[0])

survey_file.close()

# print np.asarray(Indep)
# plt.hist(SocialInfluence)
# plt.show()

pd.options.display.float_format = '${:,.2f}'.format
SurveyFrame = pd.DataFrame(np.array(Indep), columns=['Decision', 'Payback','Cost','ROI', 'Battery', 'PVDeflection','BattDeflection','MaxPay','Env2','Env1','SolarKnow', 'SolarInstalled', 'SocialInf','InterA','InterB'])
 
 
buy_logit = logit(formula = 'Decision ~ Payback  + Battery  + MaxPay + Cost ', data=SurveyFrame).fit()
print buy_logit.summary()

# logit_mod = sm.Logit(np.asarray(PurchaseChoices), np.asarray(Indep))
# logit_res = logit_mod.fit()#(disp=0)
# #print('Parameters: ', logit_res.params)
#     
# margeff = logit_res.get_margeff()
# print(margeff.summary())
# print(logit_res.summary())
# 
# Costs = []
# 
indepArray = np.array(Indep)
np.random.shuffle(indepArray)



lasso_mod = LassoLars(alpha = 0.0001)
X = indepArray[:,1:]
x_normed = X / X.max(axis=0)
y= indepArray[:,0]
lasso_res = lasso_mod.fit(x_normed, y)
lasso_coeff= lasso_res.coef_
plt.plot(abs(lasso_coeff))
#plt.scatter(np.arange(0,14), abs(lasso_coeff), labels = ['Payback','Cost','ROI', 'Battery', 'PVDeflection','BattDeflection','MaxPay','Env2','Env1','SolarKnow', 'SolarInstalled', 'SocialInf','InterA','InterB'])
plt.show()