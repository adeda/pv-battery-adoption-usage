from __future__ import division
from griddefection import *
import surveyScenarios
import owners
import outputs
from importSurvey import Indep, Respondents
# import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import pickle
import random
import csv
import pvbattsys
import copy

#Initialize Agents and Environmental Vars
NumberOfHomeOwners = len(Respondents)

ElectricityGeneration = np.zeros(8760 * SimEndDate)


#Changes: number of replicas, scenarios, threshold

SocialSizes = []
agentreplicas = 8
AllHomeOwnerIDs = np.linspace(0, (NumberOfHomeOwners-1)*agentreplicas, (NumberOfHomeOwners-1)*agentreplicas)
AllHomeOwnerIDs = AllHomeOwnerIDs.astype(int)
Ts = [0.46, 0.44, 0.42, 0.4, 0.38,0.36,0.34,0.32,0.3,0.28,0.26,0.24,0.22,0.2]
sigmas = [0.08,0.09,0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17]
AllTVars = []

for t in Ts:
    for sd in sigmas:
        AllTVars.append([t,sd])

prices = np.linspace(2,2, SimEndDate+1)
decay = []
decayfactor = 0.938

for i in range(SimEndDate+1):
    decay.append(round(prices[i]* decayfactor**i,2))
# plt.plot(decay)
# plt.show()
# print np.linspace(0.6,1,SimEndDate+1)
# print [1, .802/FiTlowPV , .802/FiTlowPV, .802/FiTlowPV,.802/FiTlowPV , \
#                         .802/FiTlowPV, .549/FiTlowPV, .549/FiTlowPV , .396/FiTlowPV, .396/FiTlowPV, .384/FiTlowPV,.384/FiTlowPV ]
ScenVars_ = [{'name': 'validation', 'BattPrice': np.asarray(decay), \
              'FiT': np.asarray([1, .802/FiTlowPV , .802/FiTlowPV, .802/FiTlowPV,.802/FiTlowPV , \
                        .802/FiTlowPV, .549/FiTlowPV, .549/FiTlowPV , .396/FiTlowPV, .396/FiTlowPV, .384/FiTlowPV,.384/FiTlowPV ]),\
            'PVPrice' :np.asarray(decay),   'ToU' : np.linspace(0.6,1,SimEndDate+1)},
              
             ]


# ScenVars_ = [{'name': 'validation', 'BattPrice': np.asarray(decay), \
#               'FiT': np.asarray([1, .802/FiTlowPV ,.802/FiTlowPV , \
#                         .802/FiTlowPV, .549/FiTlowPV , .396/FiTlowPV, .384/FiTlowPV ]),\
#             'PVPrice' :np.asarray(decay),   'ToU' : np.linspace(0.6,1,SimEndDate+1)},
#              
#              ]

#def preSim(TVars):


# plt.hist(SocialSizes)
# plt.show()
#CurrentDate = 0

#Run simulation years
print 'Number of Home Owners:', NumberOfHomeOwners*agentreplicas
def runSim(HomeOwners, CurrentScenario):
    
    
    while outputs.CurrentDate < SimEndDate:
    
        CurrentScenario.UpdateVariables()     
        #print CurrentScenario.FiT  
        #Update social network adoption fraction 
#         withPV = 0
        for ho in HomeOwners:
            ho.UpdateSocialPV(HomeOwners)
        
        for ho in HomeOwners:
            if not ho.PVBatt and ho.withSolarfrac >= ho.T:
                outputs.Adoption[outputs.CurrentDate] +=1
#             if ho.PVBatt:
#                 withPV +=1
#         populationPV = withPV/len(HomeOwners)
        #print 'Popn2' , populationPV, outputs.PVAdoptionNum[0]
            
        #Purchase PV Systems
        for ho in HomeOwners:
#             if ho.T < populationPV:
            ho.buyPV(CurrentScenario)        
        
        
                
            #Update Environmental Variables
        
        outputs.CurrentDate +=1
    
    
#     print 'PV Adoption (%)', outputs.PVAdoptionNum/FinalHO * 100
#     plt.plot(np.cumsum(outputs.PVAdoptionNum)/FinalHO * 100)
#     plt.xlabel('6-month epoch')
#     plt.ylabel('PV Adopters (%)')
#     plt.show()
    

#SimRes = [[]]* len(ScenVars_)
SimRes = []

for TVars_ in AllTVars:
    SimRun = 0
    while SimRun < 20:
    

    #Starting Values

        HomeOwners_main, PVAdNum, PVAdkW = outputs.init(TVars_, NumberOfHomeOwners, agentreplicas,'tuning')
        FinalHO =  len(HomeOwners_main)
        svcount = 0
        #print PVAdNum
        for sv in ScenVars_:
            HomeOwners_ = copy.deepcopy(HomeOwners_main)
            #Starting Values
            outputs.PVAdoptionkW = copy.deepcopy(PVAdNum) #PV Adoption in KW    
            outputs.PVAdoptionNum = copy.deepcopy(PVAdkW) #PV Adoption in number of installed systems
            outputs.CurrentDate = 0
            outputs.Adoption = np.zeros(SimEndDate+1)
            
            #print 'at start', outputs.PVAdoptionNum
            
            ToU_base = np.array((SummerToU, WinterToU))
            FiT_base = np.array((FiTlowPV, FiTmidPV))
    
            Updater= {'FiT': sv['FiT'].tolist(), 'ToU': sv['ToU'].tolist(), 'Battery': sv['BattPrice'].tolist(), 'PV': sv['PVPrice'].tolist()}
            global CurrentScen_
            BattCost = 1500
            CurrentScen_ = surveyScenarios.Scenario(FiT_base, ToU_base, 1, BattCost, Updater, sv['name'])
            
            runSim(HomeOwners_, CurrentScen_)
            for ho in HomeOwners_:
                if ho.PVBatt:
                    outputs.PVAdoptionNum[ho.PVBatt.date] +=1
#                     outputs.PVAdoptionkW[ho.PVBatt.date] += ho.PVBatt.PV
#                     if ho.PVBatt.battery >0:
#                         outputs.BatteryAdoptionkWh[ho.PVBatt.date] += ho.PVBatt.battery
#                         outputs.BatteryAdoptionNum[ho.PVBatt.date] += 1
                    
            
            
            SimRes.append({'name': sv['name'],'simrun':SimRun, 'PVNum': outputs.PVAdoptionNum, 'T': TVars_, 'Adoption': outputs.Adoption })
            
            svcount+=1
        SimRun+=1
        if SimRun == 20:
            with open('OntarioPV_'+ str(TVars_[0])+'-'+ str(TVars_[1]) + '-'+str(SimRun),'wb') as f:
                pickle.dump(SimRes, f)
